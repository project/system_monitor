## Status Page: Theme Elements
Use the Assets directory structure to fill in any necessary theme elements to
make the status page work. The majority of this should be handled with the
site's default themes.
