/**
 * @file
 * JavaScript for autologout.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attaches the batch behavior for autologout.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.autologout = {
    attach: function (context, settings) {
      if (context !== document) {
        return;
      }

      $(settings.iconElement).on('change', function () {
        var iconElement = $('#edit-icon-display span');
        if (this.value.length === 0) {
          iconElement.html(settings.iconText);
          iconElement.removeAttr('style');
          iconElement.removeClass('icon-active');
        } else {
          var iconPath = settings.iconPath + this.value;
          iconElement.html('');
          iconElement.addClass('icon-active');
          iconElement.attr('style', '-webkit-mask-image: url(' + iconPath + '); mask-image: url(' + iconPath + ');');
        }
        console.log(iconElement);
      });

    }

  };

})(jQuery, Drupal);
