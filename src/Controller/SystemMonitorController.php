<?php

namespace Drupal\system_monitor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\system_monitor\SystemMonitorInterface;

/**
 * Provides route responses for system_monitor.module.
 */
class SystemMonitorController extends ControllerBase {

  /**
   * System Monitor task configuration form title callback.
   *
   * @param \Drupal\system_monitor\SystemMonitorInterface $system_monitor
   *   The system monitor.
   *
   * @return string
   *   The title for the system monitor configure tasks form.
   */
  public function getTitle(SystemMonitorInterface $system_monitor) {
    if (is_null($system_monitor)) {
      return $this->t('No system monitor found');
    }
    return $this->t('Configure tasks for @label', ['@label' => $system_monitor->get('label')]);
  }

}
