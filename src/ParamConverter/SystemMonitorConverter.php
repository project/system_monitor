<?php

namespace Drupal\system_monitor\ParamConverter;

use Drupal\Core\ParamConverter\EntityConverter;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Parameter converter for upcasting config ids to full objects.
 *
 * This parameter converter allows for loading a system monitor object on the
 * fly in order to configure its assigned tasks.
 */
class SystemMonitorConverter extends EntityConverter implements ParamConverterInterface {

  /**
   * The type of entity to load.
   */
  const SYSTEM_MONITOR_TYPE = 'system_monitor';

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    return $this->entityTypeManager->getStorage(self::SYSTEM_MONITOR_TYPE)->load($value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] === self::SYSTEM_MONITOR_TYPE);
  }

}
