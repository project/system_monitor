<?php

namespace Drupal\system_monitor;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining System Monitor configuration entities.
 */
interface SystemMonitorInterface extends ConfigEntityInterface {

  /**
   * Get the brief description of the system monitor.
   *
   * @return string
   *   The description of the system monitor.
   */
  public function getDescription();

  /**
   * Get the tags to use when running the system monitor via drush commands.
   *
   * @param bool $return_string
   *   Whether or not to return an array or a string. Default to an array.
   *
   * @return array|string
   *   The tags for the System Monitor.
   */
  public function getMonitorTags($return_string = FALSE);

  /**
   * Get the statuses of the monitor that trigger notifications.
   *
   * @return array
   *   The array of statuses that trigger notifications for this monitor.
   */
  public function getNotifyStatuses();

}
