<?php

namespace Drupal\system_monitor\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a system monitor task annotation object.
 *
 * @see \Drupal\system_monitor\SystemMonitorTaskManager
 * @see plugin_api
 *
 * @ingroup system_monitor
 *
 * @Annotation
 */
class SystemMonitorTask extends Plugin {

  /**
   * The plugin ID for this system monitor task.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the system monitor task.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the system monitor task.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Machine name of associated Monitor.
   *
   * @var string
   */
  public $monitor_type = '';

  /**
   * A list of values used to group tasks during cron jobs.
   *
   * Allows devs to split tests across cron jobs and only run them when they
   * want rather than throwing everything into cron all the time.
   *
   * @var array
   */
  public $tags = [];

  /**
   * The amount of time to wait before retrying a failed task.
   *
   * This helps grants control and over the frequency that an endpoint of a task
   * is queried given it is in an error state. Warning states will be processed
   * as usual.
   *
   * @var int
   */
  public $retry_threshold = 120;

}
