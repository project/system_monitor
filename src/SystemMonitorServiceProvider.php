<?php

namespace Drupal\system_monitor;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\system_monitor\Compiler\SystemMonitorExtensionPass;

/**
 * Provides a service provider to register the System Monitor logging system.
 */
class SystemMonitorServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new SystemMonitorExtensionPass());
  }

}
