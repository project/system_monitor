<?php

namespace Drupal\system_monitor\EventSubscriber;

use Drupal\system_monitor\Event\MonitorEvent;
use Drupal\system_monitor\Event\SystemMonitorEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SystemMonitorLogEventSubscriber.
 *
 * @package Drupal\system_monitor\EventSubscriber
 */
class SystemMonitorLogEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      SystemMonitorEvents::OK => 'notificationOk',
      SystemMonitorEvents::WARNING => 'notificationWarning',
      SystemMonitorEvents::ERROR => 'notificationError',
    ];
  }

  /**
   * React to a OK notification.
   *
   * @param \Drupal\system_monitor\Event\MonitorEvent $event
   *   Notification event.
   */
  public function notificationOk(MonitorEvent $event) {
    // @todo
  }

  /**
   * React to a Warning notification.
   *
   * @param \Drupal\system_monitor\Event\MonitorEvent $event
   *   Notification event.
   */
  public function notificationWarning(MonitorEvent $event) {
    // @todo
  }

  /**
   * React to a Error notification.
   *
   * @param \Drupal\system_monitor\Event\MonitorEvent $event
   *   Notification event.
   */
  public function notificationError(MonitorEvent $event) {
    // @todo
  }

}
