<?php

namespace Drupal\system_monitor\Plugin\SystemMonitorTask;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\system_monitor\Logger\SystemMonitorLoggerInterface;
use Drupal\system_monitor\SystemMonitorLogLevel;
use Drupal\system_monitor\SystemMonitorTaskInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * System Monitor base class.
 */
abstract class SystemMonitorTaskBase extends PluginBase implements SystemMonitorTaskInterface, ContainerFactoryPluginInterface {

  /**
   * The system monitor logger.
   *
   * @var \Drupal\system_monitor\Logger\SystemMonitorLoggerInterface
   */
  protected $systemMonitorLogger;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\system_monitor\Logger\SystemMonitorLoggerInterface $system_monitor_logger
   *   The system monitor logger.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SystemMonitorLoggerInterface $system_monitor_logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->systemMonitorLogger = $system_monitor_logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('system_monitor_logger')
    );
  }

  /**
   * The check to run for this System Monitor task.
   *
   * @return bool
   *   Whether or not the task completed without errors or warnings.
   */
  abstract public function runTask();

  /**
   * Add in the default values for eavery log entry.
   *
   * @return array
   *   The array of log values to return
   */
  private function buildLogEntry() {
    return [
      'monitor_task_name' => $this->pluginId,
      'monitor_type' => $this->pluginDefinition['monitor_type'],
      'timestamp' => time(),
      'first_report' => 0,
    ];
  }

  /**
   * Logs the status of a new or ongoing incident.
   *
   * @param string $title
   *   The title for the log message.
   * @param string $message
   *   The detailed message for the log message.
   * @param int $level
   *   The RFC log level.
   */
  public function createOrUpdateIncident($title, $message, $level) {
    $context = ['description' => $message] + $this->buildLogEntry();
    if (!$this->hasActiveIncident() && $this->systemMonitorLogger->getSystemMonitorStatus($level) > SystemMonitorLogLevel::STATUS_OK) {
      $context['first_report'] = 1;
    }
    $this->systemMonitorLogger->log($level, $title, $context);
  }

  /**
   * Checks if there is an active incident in the called task.
   *
   * @return bool
   *   Returns the error status of the system.
   */
  public function hasActiveIncident() {
    $incidents = $this->systemMonitorLogger->getLatestMonitorStatusKeyedByMonitor();
    $has_incident = FALSE;
    if (!empty($incidents[$this->pluginDefinition['monitor_type']][$this->pluginId])) {
      $incident_log = $incidents[$this->pluginDefinition['monitor_type']][$this->pluginId];
      if ($incident_log['status'] > SystemMonitorLogLevel::STATUS_OK) {
        $has_incident = TRUE;
      }
    }
    return $has_incident;
  }

}
