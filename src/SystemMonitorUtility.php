<?php

namespace Drupal\system_monitor;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\system_monitor\Logger\SystemMonitorLogger;

/**
 * Provides utility functions to interact with the logging backend.
 */
class SystemMonitorUtility {

  use StringTranslationTrait;

  /**
   * Cache service object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The menu link overrides config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The system monitor logger.
   *
   * @var \Drupal\system_monitor\Logger\SystemMonitorLogger
   */
  protected $systemMonitorLogger;

  /**
   * The system monitor task manager.
   *
   * @var \Drupal\system_monitor\SystemMonitorTaskManager
   */
  protected $systemMonitorTaskManager;

  /**
   * Constructs a SystemMonitorUtility object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger interface.
   * @param \Drupal\system_monitor\Logger\SystemMonitorLogger $system_monitor_logger
   *   The system monitor logger service.
   * @param \Drupal\system_monitor\SystemMonitorTaskManager $system_monitor_task_manager
   *   The system monitor task manager.
   */
  public function __construct(CacheBackendInterface $cache, Connection $connection, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger, SystemMonitorLogger $system_monitor_logger, SystemMonitorTaskManager $system_monitor_task_manager) {
    $this->cache = $cache;
    $this->configFactory = $config_factory;
    $this->config = $config_factory->get('system_monitor.config');
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger->get('system_monitor');
    $this->systemMonitorLogger = $system_monitor_logger;
    $this->systemMonitorTaskManager = $system_monitor_task_manager;
  }

  /**
   * Determines if there are any active incidents on the system.
   *
   * @return bool
   *   Whether there are active incidents on the system.
   */
  public function hasActiveIncident() {
    $statuses = $this->systemMonitorLogger->getLatestMonitorStatusKeyedByMonitor();
    foreach ($statuses as $tasks) {
      foreach ($tasks as $task) {
        if ($task['status'] > 1) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Gathers status information for the identified monitors.
   *
   * @param \Drupal\system_monitor\SystemMonitorInterface[] $monitors
   *   The array of monitor ids to use when gathering log data.
   * @param bool $use_state
   *   Whether to use or rebuild the state of the incident data in the system.
   *
   * @return array
   *   The array of incident data for current incidents (if any).
   */
  public function getIncidentData(array $monitors, $use_state = TRUE) {
    $incidents = $this->systemMonitorLogger->getLatestMonitorStatusKeyedByMonitor();

    $build = [];
    foreach ($monitors as $monitor) {
      $cache_key = 'system_monitor.' . $monitor->id() . '.incident_data';
      $cached_monitor_card = $this->cache->get($cache_key);
      if (!empty($cached_monitor_card)) {
        $build[$monitor->id()] = $cached_monitor_card->data;
        continue;
      }
      $monitor_status = SystemMonitorLogLevel::STATUS_OK;
      $icon = $this->getFullIconPath($monitor->getIcon());
      $monitor_build = [
        'title' => $monitor->label(),
        'image' => $icon,
        'detail' => [
          'description' => $monitor->getDescription(),
        ],
      ];

      $tasks = $this->systemMonitorTaskManager->getAvailableTasksByMonitor($monitor);

      // Don't show the monitor on the status page if it doesn't have any tasks.
      if (empty($tasks)) {
        continue;
      }

      foreach ($tasks as $id => $task) {
        if (isset($incidents[$task['monitor_type']][$id])) {
          $incident_code = (int) $incidents[$task['monitor_type']][$id]['status'];
          if ($incident_code > $monitor_status) {
            $monitor_status = $incident_code;
          }
        }
        $monitor_build['detail']['tasks'][] = [
          'title' => $task['label'],
          'status' => $incident_code,
        ];
      }
      $monitor_build['status'] = $monitor_status;
      $this->cache->set($cache_key, $monitor_build, Cache::PERMANENT, [
        'system_monitor',
        'system_monitor.' . $monitor->id(),
      ]);
      $build[$monitor->id()] = $monitor_build;
    }

    return $build;

  }

  /**
   * Gets the IDs for all system monitors that are active in the system.
   *
   * @return array
   *   An array of monitor IDs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getActiveMonitorIds() {
    return $this->entityTypeManager
      ->getStorage('system_monitor')
      ->getQuery()
      ->condition('status', TRUE)
      ->execute();
  }

  /**
   * Gets the ID => Title pairs for all active system monitors.
   *
   * @return array
   *   An array of monitor values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getActiveMonitorOptions() {
    $system_monitor_ids = $this->getActiveMonitorIds();
    $monitors = $this->entityTypeManager->getStorage('system_monitor')
      ->loadMultiple($system_monitor_ids);
    $options = [];
    foreach ($monitors as $id => $monitor) {
      $options[$id] = $monitor->label();
    }
    return $options;
  }

  /**
   * Builds the full path to the icon in the system for inclusion in the theme.
   *
   * @param string $partial_path
   *   The subfolder/filename partial page for the icon file.
   *
   * @return string
   *   The full server path to the icon file.
   */
  public function getFullIconPath($partial_path) {
    $module_path = drupal_get_path('module', 'system_monitor');
    return '/' . $module_path . '/assets/images/icons/' . $partial_path;
  }

  /**
   * Loads the names and strucure of the system monitor icons for status pages.
   */
  public function getIconOptionsList() {
    $icon_files = $this->loadIconData();
    $icon_options = [];
    foreach ($icon_files as $directory => $files) {
      $group_name = '';
      foreach ($files as $key => $file_name) {
        if ($key === 0) {
          $group_name = str_replace('-', ' ', $directory);
          $group_name = ucwords($group_name);
          $icon_options[$group_name] = [];
        }
        // Filter out the files that are just the full list of icons.
        if (strpos($file_name, '-icons.') === FALSE) {
          $file_label = substr($file_name, 0, strpos($file_name, '.'));
          $file_label = str_replace('-', ' ', $file_label);
          $file_label = ucwords($file_label);
          $icon_options[$group_name][$directory . '/' . $file_name] = $file_label;
        }
      }
    }
    return $icon_options;
  }

  /**
   * Loads the names and strucure of the system monitor icons for status pages.
   */
  public function loadIconData() {
    $module_path = drupal_get_path('module', 'system_monitor');
    $icon_files = [];
    $this->findFilePaths($module_path . '/assets/images/icons', $icon_files);
    $icon_files = array_filter($icon_files, function ($a) {
      return is_array($a);
    });
    return $icon_files;
  }

  /**
   * Function to load a directory and it's full file structure.
   *
   * @param string $path
   *   The path to scan.
   * @param array $directory_structure
   *   The list of files and directories.
   *
   * @return array|false
   *   An array of path/file values or FALSE if nothing is found.
   *
   * @see https://www.tutorialrepublic.com/php-tutorial/php-parsing-directories.php
   */
  private function findFilePaths($path, array &$directory_structure) {
    // Check directory exists or not.
    if (file_exists($path) && is_dir($path)) {
      // Scan the files in this directory.
      $result = scandir($path);

      // Filter out the current (.) and parent (..) directories.
      $files = array_diff($result, ['.', '..']);

      if (count($files) > 0) {
        // Loop through retuned array.
        foreach ($files as $file) {
          if (is_file("$path/$file")) {
            $directory_structure[] = $file;
          }
          elseif (is_dir("$path/$file")) {
            $directory_structure[$file] = [];
            $this->findFilePaths("$path/$file", $directory_structure[$file]);
          }
        }
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }

  }

  /**
   * Add additional logging when debug mode is enabled.
   *
   * @param string $message
   *   The message to log.
   * @param array $params
   *   Any additional values to log, such as a request payload.
   *
   * @throws \Drupal\Core\Database\DatabaseException
   */
  public function logDebugMessage($message, array $params = []) {
    if (!empty($this->config->get('debug'))) {
      if (!empty($params)) {
        $this->logger->log('info', $this->t('@message @params', [
          '@message' => $message,
          '@params' => print_r($params, TRUE),
        ]));
      }
      else {
        $this->logger->log('info', $this->t('@message', [
          '@message' => $message,
        ]));
      }
    }
  }

}
