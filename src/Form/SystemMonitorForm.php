<?php

namespace Drupal\system_monitor\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system_monitor\SystemMonitorLogLevel;
use Drupal\system_monitor\SystemMonitorUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SystemMonitorForm.
 */
class SystemMonitorForm extends EntityForm {

  /**
   * The system monitor utility service.
   *
   * @var \Drupal\system_monitor\SystemMonitorUtility
   */
  protected $systemMonitorUtility;

  /**
   * Constructs a new StatusPageForm.
   *
   * @param \Drupal\system_monitor\SystemMonitorUtility $system_monitor_utility
   *   The system monitor utility.
   */
  public function __construct(SystemMonitorUtility $system_monitor_utility) {
    $this->systemMonitorUtility = $system_monitor_utility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('system_monitor.utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $system_monitor = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $system_monitor->label(),
      '#description' => $this->t('Label for the System Monitor configuration.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $system_monitor->id(),
      '#machine_name' => [
        'exists' => '\Drupal\system_monitor\Entity\SystemMonitor::load',
      ],
      '#disabled' => !$system_monitor->isNew(),
    ];

    $icon_placeholder = $this->t('No Icon Selected');
    $icon_text = $icon_placeholder;
    $icon_attributes = [];
    if (!empty($system_monitor->getIcon())) {
      $icon_path = $this->systemMonitorUtility->getFullIconPath($system_monitor->getIcon());
      $icon_text = '';
      $icon_attributes = [
        'class' => [
          'icon-active',
        ],
        'style' => '-webkit-mask-image: url(' . $icon_path . '); mask-image: url(' . $icon_path . ');">',
      ];
    }
    $form['icon_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'icon-wrapper',
        ],
      ],
      '#attached' => [
        'library' => [
          'system_monitor/system_monitor.admin',
        ],
        'drupalSettings' => [
          'iconPath' => $this->systemMonitorUtility->getFullIconPath(''),
          'iconText' => $icon_placeholder,
          'iconElement' => '#edit-icon',
        ],
      ],
      'icon_display' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'icon-display-wrapper',
          ],
        ],
        'icon_text' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $icon_text,
          '#attributes' => $icon_attributes,
        ],
      ],
      'icon' => [
        '#type' => 'select',
        '#title' => $this->t('Icon'),
        '#default_value' => $system_monitor->getIcon(),
        '#description' => $this->t("Select an icon for this monitor."),
        '#options' => $this->systemMonitorUtility->getIconOptionsList(),
        '#required' => TRUE,
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#rows' => 2,
      '#default_value' => $system_monitor->getDescription(),
      '#description' => $this->t('Description of the System Monitor configuration.'),
      '#required' => TRUE,
    ];

    $form['tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tags'),
      '#default_value' => $system_monitor->getMonitorTags(TRUE),
      '#description' => $this->t('Tags to use when running this system monitor via drush commands. Enter one tag per line.'),
      '#required' => FALSE,
    ];

    // Filter out unselected options since they are represented as a zero value
    // which confuses the system since "Pending" has a status integer value of
    // zero.
    $default_statuses = array_filter($system_monitor->getNotifyStatuses(), function ($value) {
      return !is_int($value);
    });
    // @todo Update options once Event logic is in place.
    $form['notify_statuses'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Notify Statuses'),
      '#options' => SystemMonitorLogLevel::getStatuses(),
      '#default_value' => $default_statuses,
      '#description' => $this->t('The monitor statuses that trigger notifications for this monitor.'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $system_monitor->status(),
      '#description' => $this->t('Uncheck this to disable this system monitor and all of its tasks.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $system_monitor = $this->entity;
    $tags = $form_state->getValue('tags');
    if (!empty($tags)) {
      $tags = explode("\n", $tags);
      $tags = array_map('trim', $tags);
      $system_monitor->set('tags', $tags);
    }
    else {
      $system_monitor->set('tags', []);
    }
    $status = $system_monitor->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label System Monitor configuration.', [
          '%label' => $system_monitor->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label System Monitor configuration.', [
          '%label' => $system_monitor->label(),
        ]));
    }
    $form_state->setRedirectUrl($system_monitor->toUrl('collection'));
  }

}
