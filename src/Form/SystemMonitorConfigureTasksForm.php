<?php

namespace Drupal\system_monitor\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system_monitor\SystemMonitorInterface;
use Drupal\system_monitor\SystemMonitorTaskManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for System Monitor tasks associated with a given monitor.
 */
class SystemMonitorConfigureTasksForm extends ConfigFormBase {

  /**
   * The system monitor to use when configuring tasks.
   *
   * @var \Drupal\system_monitor\SystemMonitorInterface
   */
  protected $systemMonitor;

  /**
   * System monitor utility object.
   *
   * @var \Drupal\system_monitor\SystemMonitorTaskManager
   */
  protected $systemMonitorTaskManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, SystemMonitorTaskManager $system_monitor_task_manager) {
    parent::__construct($config_factory);
    $this->systemMonitorTaskManager = $system_monitor_task_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.system_monitor_task')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'system_monitor_configure_tasks_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SystemMonitorInterface $system_monitor = NULL) {
    if (is_null($system_monitor)) {
      return [
        'error' => [
          '#type' => 'markup',
          '#markup' => $this->t('No system monitor found to edit. Please try again with a different monitor.'),
        ],
      ];
    }
    else {
      $this->systemMonitor = $system_monitor;
    }
    $form = parent::buildForm($form, $form_state);

    $form['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('Select which tasks to run when this monitor is executed. <em>If nothing is selected then all tasks will be run.</em>'),
    ];

    $available_tasks = $this->systemMonitorTaskManager->getAvailableTasksByMonitor($this->systemMonitor, [], FALSE, FALSE);
    $options = [];
    foreach ($available_tasks as $key => $definition) {
      $options[$key] = [
        'task_id' => $key,
        'task_label' => $definition['label'],
        'task_description' => $definition['description'],
      ];
    }
    $default_values = $this->config($this->systemMonitor->getTaskConfigId())->get('available_tasks');

    $form['available_tasks'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Available tasks'),
      '#header' => [
        'task_id' => $this->t('Task id'),
        'task_label' => $this->t('Task label'),
        'task_description' => $this->t('Task description'),
      ],
      '#default_value' => !empty($default_values) ? $default_values : [],
      '#options' => $options,
      '#empty' => $this->t('No tasks found for this monitor.'),
    ];

    if (empty($options)) {
      unset($form['actions']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config_id = $this->systemMonitor->getTaskConfigId();
    $monitor_task_settings = $this->config($config_id);

    $monitor_task_settings->set('available_tasks', $values['available_tasks']);
    $monitor_task_settings->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Return the configuration names.
   */
  protected function getEditableConfigNames() {
    return [
      $this->systemMonitor->getTaskConfigId(),
    ];
  }

}
