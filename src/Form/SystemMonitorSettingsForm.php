<?php

namespace Drupal\system_monitor\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\system_monitor\SystemMonitorUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * System monitor config form.
 */
class SystemMonitorSettingsForm extends ConfigFormBase {

  /**
   * Configuration name.
   */
  const CONFIG_NAME = 'system_monitor.config';

  /**
   * State service object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The system monitor utility service.
   *
   * @var \Drupal\system_monitor\SystemMonitorUtility
   */
  protected $systemMonitorUtility;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state, SystemMonitorUtility $system_monitor_utility) {
    parent::__construct($config_factory);
    $this->state = $state;
    $this->systemMonitorUtility = $system_monitor_utility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('system_monitor.utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'system_monitor_config_form';
  }

  /**
   * Return the configuration names.
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $icon_placeholder = $this->t('No Icon Selected');
    $icon_text = $icon_placeholder;
    $icon_attributes = [];
    if (!empty($config->get('default_icon'))) {
      $icon_path = $this->systemMonitorUtility->getFullIconPath($config->get('default_icon'));
      $icon_text = '';
      $icon_attributes = [
        'class' => [
          'icon-active',
        ],
        'style' => '-webkit-mask-image: url(/' . $icon_path . '); mask-image: url(/' . $icon_path . ');">',
      ];
    }
    $form['icon_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'icon-wrapper',
        ],
      ],
      '#attached' => [
        'library' => [
          'system_monitor/system_monitor.admin',
        ],
        'drupalSettings' => [
          'iconPath' => $this->systemMonitorUtility->getFullIconPath(''),
          'iconText' => $icon_placeholder,
          'iconElement' => '#edit-default-icon',
        ],
      ],
      'icon_display' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'icon-display-wrapper',
          ],
        ],
        'icon_text' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $icon_text,
          '#attributes' => $icon_attributes,
        ],
      ],
      'default_icon' => [
        '#type' => 'select',
        '#title' => $this->t('Default System Monitor Icon'),
        '#default_value' => $config->get('default_icon'),
        '#description' => $this->t("Select a defaut icon for they system to use when one hasn't bee set for a monitor."),
        '#options' => $this->systemMonitorUtility->getIconOptionsList(),
        '#required' => TRUE,
      ],
    ];

    $form['log_expiration'] = [
      '#type' => 'select',
      '#title' => $this->t('How long should logs be tracked?'),
      '#required' => TRUE,
      '#options' => [
        '1 year' => $this->t('1 year'),
        '6 months' => $this->t('6 months'),
        '3 months' => $this->t('3 months'),
        '1 month' => $this->t('1 month'),
        '7 days' => $this->t('1 week'),
      ],
      '#default_value' => $config->get('log_expiration') ? $config->get('log_expiration') : '1 year',
      '#description' => $this->t('System logs will be truncated during cron runs once a day to prevent the database from filling with log values.'),
    ];

    $form['cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Run system monitors on cron'),
      '#description' => $this->t('Warning: only enable it if monitoring a few processes. Otherwise you should consider using the drush command implementation.'),
      '#default_value' => $config->get('cron'),
    ];

    $form['debugging'] = [
      '#type' => 'details',
      '#title' => $this->t('Debugging'),
      'debug' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable debug mode'),
        '#description' => $this->t('Warning: only enable in NON-production sites as this will create verbose messaging.'),
        '#default_value' => $config->get('debug'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::CONFIG_NAME)
      ->set('debug', $form_state->getValue('debug'))
      ->set('default_icon', $form_state->getValue('default_icon'))
      ->set('cron', $form_state->getValue('cron'))
      ->set('log_expiration', $form_state->getValue('log_expiration'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
