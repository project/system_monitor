<?php

namespace Drupal\system_monitor;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * The base System Monitor Task Interface.
 */
interface SystemMonitorTaskInterface extends PluginInspectionInterface {

  /**
   * The check to run for this System Monitor task.
   *
   * @return int
   *   The status value returned by the monitor task.
   */
  public function runTask();

}
