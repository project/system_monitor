<?php

namespace Drupal\system_monitor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Traversable;

/**
 * System Monitor Task Manager Service.
 */
class SystemMonitorTaskManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * The cache data interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The cache tags invalidator interface.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a SystemMonitorTaskManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_data
   *   Cache data bin instance to use.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invaliator
   *   The cache tags invalidator interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger interface.
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_data, CacheTagsInvalidatorInterface $cache_tags_invalidator, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger) {
    parent::__construct(
      'Plugin/SystemMonitorTask',
      $namespaces,
      $module_handler,
      'Drupal\system_monitor\SystemMonitorTaskInterface',
      'Drupal\system_monitor\Annotation\SystemMonitorTask'
    );
    $this->alterInfo('system_monitor');
    $this->setCacheBackend($cache_backend, 'system_monitor_plugins');
    $this->cache = $cache_data;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger->get('system_monitor');
  }

  /**
   * The primary function that runs the system's configured tasks.
   *
   * @param array $monitor_ids
   *   The types of monitors to run. If not set, run them all.
   * @param array $monitor_tags
   *   The tags to use when running monitors. If not set, run them all.
   * @param array $task_tags
   *   The tags to use when running tasks. If not set, run them all.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function runSystemMonitorTasks(array $monitor_ids = [], array $monitor_tags = [], array $task_tags = []) {
    $storage = $this->entityTypeManager->getStorage('system_monitor');
    $query = $storage->getQuery()
      ->condition('status', 1);
    if (!empty($monitor_tags)) {
      $query->condition('tags', $monitor_tags, 'IN');
    }
    if (!empty($monitor_ids)) {
      $query->condition('id', $monitor_ids, 'IN');
    }
    $monitor_results = $query->execute();

    if (empty($monitor_results)) {
      $this->logger->info($this->t('No monitors could be run because no active monitors were found.'));
      return;
    }

    $monitors = $storage->loadMultiple($monitor_results);
    foreach ($monitors as $monitor_id => $monitor) {
      $tasks = $this->getAvailableTasksByMonitor($monitor, $task_tags);
      foreach ($tasks as $task_id => $task_definition) {
        $task = $this->createInstance($task_id);
        // Wrap the task runner in a try/catch out of an overabundance of
        // caution. This should be a last resort when running tasks in the
        // system.
        try {
          $task->runTask();
        }
        catch (\Exception $e) {
          watchdog_exception('system_monitor', $e);
        }
      }
      $this->cacheTagsInvalidator->invalidateTags(['system_monitor.' . $monitor_id]);
    }

    // Track the timestamp when monitor tasks were last updated.
    $this->cache->set('system_monitor.last_updated', time(), CacheBackendInterface::CACHE_PERMANENT, [
      'system_monitor',
      'system_monitor.last_updated',
    ]);
  }

  /**
   * Performs a lookup of all available system monitor tasks by type.
   *
   * @param \Drupal\system_monitor\Entity\SystemMonitorInterface $monitor
   *   The system monitor entity.
   * @param array $task_tags
   *   The tags to use when running tasks. If not set, run them all.
   * @param bool $options_list
   *   If true, then the results are returned formatted in key => value pairs.
   * @param bool $filtered
   *   If true, then return filtered config to those set to run on a monitor.
   *
   * @return array
   *   The array of task definitions keyed by task_id.
   */
  public function getAvailableTasksByMonitor(SystemMonitorInterface $monitor, array $task_tags = [], $options_list = FALSE, $filtered = TRUE) {
    $definitions = $this->getDefinitions();
    $options = [];

    // Filter the tasks returned based on if they are configured as active
    // against the monitor.
    if ($filtered) {
      $config_id = $monitor->getTaskConfigId();
      $available_tasks = $this->configFactory->get($config_id)->get('available_tasks');
      if (!empty($available_tasks)) {
        $available_tasks = array_filter($available_tasks);
        $definitions = array_intersect_key($definitions, $available_tasks);
      }
    }

    if (!empty($task_tags)) {
      $filtered_definitions = array_filter($definitions, function ($task_definition) use ($task_tags) {
        if (isset($task_definition['tags'])) {
          $tags = $task_definition['tags'];
          $tags = explode(',', $tags);
          return array_intersect($tags, $task_tags);
        }
        return FALSE;
      });
    }
    else {
      $id = $monitor->id();
      $filtered_definitions = array_filter($definitions, function ($task_definition) use ($id) {
        return isset($task_definition['monitor_type']) && $task_definition['monitor_type'] == $id;
      });
    }
    if ($options_list) {
      foreach ($filtered_definitions as $task_id => $task_definition) {
        $options[$task_id] = $task_definition['label'];
      }
    }
    else {
      $options = $filtered_definitions;
    }
    if (!empty($options)) {
      ksort($options);
    }
    return $options;
  }

}
