<?php

namespace Drupal\system_monitor;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of System Monitor configuration entities.
 */
class SystemMonitorListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Machine name');
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');
    $header['tags'] = $this->t('Tags');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['description'] = $entity->getDescription();
    $row['tags']['data'] = [
      '#theme' => 'item_list',
      '#items' => $entity->getMonitorTags(),
    ];
    $row['status'] = empty($entity->status()) ? $this->t('Disabled') : $this->t('Enabled');
    return $row + parent::buildRow($entity);
  }

  /**
   * Gets this list's default operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the operations are for.
   *
   * @return array
   *   The array structure is identical to the return value of
   *   self::getOperations().
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $configure_url = Url::fromRoute('system_monitor.config_tasks', ['system_monitor' => $entity->id()]);
    $operations['configure'] = [
      'title' => $this->t('Configure'),
      'weight' => 50,
      'url' => $this->ensureDestination($configure_url),
    ];
    return $operations;
  }

}
