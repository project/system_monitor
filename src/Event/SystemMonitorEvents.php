<?php

namespace Drupal\system_monitor\Event;

/**
 * System Monitor Event class.
 */
final class SystemMonitorEvents {

  /**
   * Name of the event fired when notification with "ok" status.
   *
   * @Event
   *
   * @see \Drupal\system_monitor\Event\MonitorEvent
   *
   * @var string
   */
  const OK = 'system_monitor.ok';

  /**
   * Name of the event fired when notification with "warning" status.
   *
   * @Event
   *
   * @see \Drupal\system_monitor\Event\MonitorEvent
   *
   * @var string
   */
  const WARNING = 'system_monitor.warning';


  /**
   * Name of the event fired when notification with "error" status.
   *
   * @Event
   *
   * @see \Drupal\system_monitor\Event\MonitorEvent
   *
   * @var string
   */
  const ERROR = 'system_monitor.error';

}
