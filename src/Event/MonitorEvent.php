<?php

namespace Drupal\system_monitor\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Wraps a system monitor report event for event subscribers.
 */
class MonitorEvent extends Event {

  /**
   * Notification message.
   *
   * @var string
   */
  protected $message;

  /**
   * Notification Task.
   *
   * @var string
   */
  protected $task;

  /**
   * Constructs an notification event object.
   *
   * @param string $message
   *   The message of the notification.
   * @param string $task
   *   The system monitor task.
   */
  public function __construct($message, $task) {
    $this->message = $message;
    $this->task = $task;
  }

  /**
   * Get the message.
   *
   * @return string
   *   The message for notification.
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Get the monitor task.
   *
   * @return string
   *   The task of the monitor.
   */
  public function getTask() {
    return $this->task;
  }

}
