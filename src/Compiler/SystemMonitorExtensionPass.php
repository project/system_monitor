<?php

namespace Drupal\system_monitor\Compiler;

use Drupal\system_monitor\Logger\SystemMonitorLogger;
use Drupal\system_monitor\Logger\SystemMonitorLoggerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Compiler pass for the custom System Monitor logging system.
 */
class SystemMonitorExtensionPass implements CompilerPassInterface {

  /**
   * The tag to use on services to enable custom logging configuration.
   *
   * @var string
   */
  const SYSTEM_MONITOR_LOGGER_TAG = 'system_monitor_logger';

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    $container
      ->setAlias(SystemMonitorLoggerInterface::class, self::SYSTEM_MONITOR_LOGGER_TAG)
      ->setPublic(FALSE);
    if ($container->has(self::SYSTEM_MONITOR_LOGGER_TAG)) {
      return;
    }
    $container
      ->register(self::SYSTEM_MONITOR_LOGGER_TAG, SystemMonitorLogger::class)
      ->setPublic(FALSE);
  }

}
