<?php

namespace Drupal\system_monitor\Logger;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseException;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

/**
 * Logs events in the system_monitor_log database table.
 *
 * @see Drupal\dblog\Logger\DbLog
 *   Heavily borrowed from dblog (watchdog).
 */
class SystemMonitorDbLogger implements LoggerInterface {

  use LoggerTrait;
  use SystemMonitorLoggerTrait;

  /**
   * The dedicated database connection target to use for log entries.
   */
  const DEDICATED_SYSTEM_MONITOR_LOG_CONNECTION_TARGET = 'dedicated_system_monitor_log';

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a SystemMonitorLogger object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    // Automatically return if this isn't a System Monitor logged event.
    if (!isset($context['monitor_task_name']) || !isset($context['monitor_type'])) {
      return;
    }

    // Remove any backtraces since they may contain an unserializable variable.
    unset($context['backtrace']);

    if (isset($context['status'])) {
      $status = $context['status'];
    }
    else {
      // Get the System Monitor status level based on the RFC severity level
      // that was passed through to the log function.
      $status = $this->getSystemMonitorStatus($level);
    }

    try {
      $this->connection
        ->insert('system_monitor_log')
        ->fields([
          'monitor_task_name' => $context['monitor_task_name'],
          'monitor_type' => $context['monitor_type'],
          'short_description' => $message,
          'description' => !empty($context['description']) ? $context['description'] : NULL,
          'severity' => $level,
          'status' => $status,
          'timestamp' => $context['timestamp'],
          'first_report' => !empty($context['first_report']) ? 1 : 0,
        ])
        ->execute();
    }
    catch (\Exception $e) {
      // When running Drupal on MySQL or MariaDB you can run into several errors
      // that corrupt the database connection. Some examples for these kind of
      // errors on the database layer are "1100 - Table 'xyz' was not locked
      // with LOCK TABLES" and "1153 - Got a packet bigger than
      // 'max_allowed_packet' bytes". If such an error happens, the MySQL server
      // invalidates the connection and answers all further requests in this
      // connection with "2006 - MySQL server had gone away". In that case the
      // insert statement above results in a database exception. To ensure that
      // the causal error is written to the log we try once to open a dedicated
      // connection and write again.
      if (
        // Only handle database related exceptions.
        ($e instanceof DatabaseException || $e instanceof \PDOException) &&
        // Avoid an endless loop of re-write attempts.
        $this->connection->getTarget() != self::DEDICATED_SYSTEM_MONITOR_LOG_CONNECTION_TARGET
      ) {
        // Open a dedicated connection for logging.
        $key = $this->connection->getKey();
        $info = Database::getConnectionInfo($key);
        Database::addConnectionInfo($key, self::DEDICATED_SYSTEM_MONITOR_LOG_CONNECTION_TARGET, $info['default']);
        $this->connection = Database::getConnection(self::DEDICATED_SYSTEM_MONITOR_LOG_CONNECTION_TARGET, $key);
        // Now try once to log the error again.
        $this->log($level, $message, $context);
      }
      else {
        throw $e;
      }
    }
  }

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   * @param int $start_timestamp
   *   The timestamp to use to cut off historical log records.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function buildHistoricalEventsByMonitor(array $monitors = [], $start_timestamp = 0) {
    $today = strtotime('midnight');
    $query = $this->connection->select('system_monitor_log', 'l')
      ->fields('l')
      ->condition('timestamp', $today, '<')
      ->condition('timestamp', $start_timestamp, '>=');
    if (!empty($monitors)) {
      $query->condition('monitor_type', $monitors, 'IN');
    }
    $query->orderBy('timestamp', 'ASC');
    $logs = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    // Build the events array structure for the timeline.
    return $this->reorderLogData($logs);
  }

  /**
   * Get the latest monitor logs and organize them by monitor and task.
   *
   * @return array
   *   The array of latest messages for each monitor.
   */
  public function getLatestMonitorStatusKeyedByMonitor() {
    $logs = $this->getLatestMonitorStatus();
    $keyed_logs = [];
    foreach ($logs as $log) {
      $keyed_logs[$log['monitor_type']][$log['monitor_task_name']] = $log;
    }
    return $keyed_logs;
  }

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function getLatestMonitorStatus(array $monitors = []) {
    $query = $this->connection->select('system_monitor_log', 'l')
      ->fields('l');
    $subquery = $this->connection->select('system_monitor_log', 'sml')
      ->fields('sml', ['monitor_task_name', 'monitor_type']);
    if (!empty($monitors)) {
      $subquery->condition('monitor_type', $monitors, 'IN');
    }
    $subquery->addExpression('MAX(sml.timestamp)', 'timestamp');
    $subquery->groupBy('sml.monitor_task_name');
    $subquery->groupBy('sml.monitor_type');
    $query->join($subquery, 't', 't.monitor_task_name = l.monitor_task_name AND t.monitor_type = l.monitor_type AND t.timestamp = l.timestamp');
    return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Find log records for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function getMonitorLogs(array $monitors = []) {
    $this_week = strtotime('now -30 days');
    $query = $this->connection->select('system_monitor_log', 'l')
      ->fields('l')
      ->condition('timestamp', $this_week, '>=');
    if (!empty($monitors)) {
      $query->condition('monitor_type', $monitors, 'IN');
    }
    $query->orderBy('timestamp', 'ASC');
    $logs = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    // Build the events array structure for the timeline.
    return $this->reorderLogData($logs);
  }

}
