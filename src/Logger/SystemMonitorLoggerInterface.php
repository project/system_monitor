<?php

namespace Drupal\system_monitor\Logger;

use Psr\Log\LoggerInterface;

/**
 * Defines an interface for logging system monitor events.
 */
interface SystemMonitorLoggerInterface {

  /**
   * Add a log entry for a new issue or as an update to an existing issue.
   *
   * This need not be confimed to only be run during an automated cron job. It
   * could be incorporated into the system in other places, similar to exception
   * logging, to trigger system monitor events.
   *
   * @param int $level
   *   The RFC severity level.
   * @param string $message
   *   The short message to include in the log entry.
   * @param array $context
   *   The context array of associated values for the log entry.
   *
   * @throws \Drupal\Core\Database\DatabaseException
   */
  public function log($level, $message, array $context = []);

  /**
   * Sets the loggers for this channel.
   *
   * @param array $loggers
   *   An array of arrays of \Psr\Log\LoggerInterface keyed by priority.
   */
  public function setLoggers(array $loggers);

  /**
   * Adds a logger to all the channels.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The PSR-3 logger to add.
   * @param int $priority
   *   The priority of the logger being added.
   *
   * @see \Drupal\Core\DependencyInjection\Compiler\RegisterLoggersPass
   */
  public function addLogger(LoggerInterface $logger, $priority = 0);

  /**
   * Get the primary logger for System Monitor and use it to build log records.
   *
   * @return null|\Psr\Log\LoggerInterface
   *   Either the primary logger interface, or null.
   */
  public function getPrimaryLogger();

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   * @param int $start_timestamp
   *   The timestamp to use to cut off historical log records.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function buildHistoricalEventsByMonitor(array $monitors = [], $start_timestamp = 0);

  /**
   * Get the latest monitor logs and organize them by monitor and task.
   *
   * @return array
   *   The array of latest messages for each monitor.
   */
  public function getLatestMonitorStatusKeyedByMonitor();

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function getLatestMonitorStatus(array $monitors = []);

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   * @param int $limit
   *   The number of logs to return.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function getMonitorLogs(array $monitors = [], $limit = 10);

}
