<?php

namespace Drupal\system_monitor\Logger;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Defines a logger system for logging system monitor events.
 */
class SystemMonitorLogger implements SystemMonitorLoggerInterface, ContainerAwareInterface {
  use ContainerAwareTrait;
  use LoggerTrait;
  use SystemMonitorLoggerTrait;

  /**
   * Maximum call depth to self::log() for a single log message.
   *
   * It's very easy for logging channel code to call out to other library code
   * that will create log messages. In that case, we will recurse back in to
   * LoggerChannel::log() multiple times while processing a single originating
   * message. To prevent infinite recursion, we track the call depth and bail
   * out at LoggerChannel::MAX_CALL_DEPTH iterations.
   *
   * @var int
   */
  const MAX_CALL_DEPTH = 5;

  /**
   * Number of times LoggerChannel::log() has been called for a single message.
   *
   * @var int
   */
  protected $callDepth = 0;

  /**
   * An array of arrays of \Psr\Log\LoggerInterface keyed by priority.
   *
   * @var array
   */
  protected $loggers = [];

  /**
   * Get the primary logger for System Monitor and use it to build log records.
   *
   * @return null|\Psr\Log\LoggerInterface
   *   Either the primary logger interface, or null.
   */
  public function getPrimaryLogger() {
    $logger = NULL;
    if (!empty($this->loggers)) {
      ksort($this->loggers);
      $loggers = current($this->loggers);
      if (!empty($loggers)) {
        $logger = current($loggers);
      }
    }
    return $logger;
  }

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   * @param int $start_timestamp
   *   The timestamp to use to cut off historical log records.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function buildHistoricalEventsByMonitor(array $monitors = [], $start_timestamp = 0) {
    $logger = $this->getPrimaryLogger();
    $logs = [];
    if (!empty($logger)) {
      $logs = $logger->buildHistoricalEventsByMonitor($monitors, $start_timestamp);
    }
    return $logs;
  }

  /**
   * Get the latest monitor logs and organize them by monitor and task.
   *
   * @return array
   *   The array of latest messages for each monitor.
   */
  public function getLatestMonitorStatusKeyedByMonitor() {
    $logger = $this->getPrimaryLogger();
    $logs = [];
    if (!empty($logger)) {
      $logs = $logger->getLatestMonitorStatusKeyedByMonitor();
    }
    return $logs;
  }

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function getLatestMonitorStatus(array $monitors = []) {
    $logger = $this->getPrimaryLogger();
    $logs = [];
    if (!empty($logger)) {
      $logs = $logger->getLatestMonitorStatus($monitors);
    }
    return $logs;
  }

  /**
   * Find all the records that are the latest for each monitored task.
   *
   * @param array $monitors
   *   The monitors to track with this query.
   * @param int $limit
   *   The number of logs to return.
   *
   * @return array
   *   An array of the latest messages for monitored tasks.
   */
  public function getMonitorLogs(array $monitors = [], $limit = 10) {
    $logger = $this->getPrimaryLogger();
    $logs = [];
    if (!empty($logger)) {
      $logs = $logger->getMonitorLogs($monitors, $limit);
    }
    return $logs;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    if ($this->callDepth == self::MAX_CALL_DEPTH) {
      return;
    }
    $this->callDepth++;

    // Call all available loggers.
    foreach ($this->sortLoggers() as $logger) {
      $logger->log($level, $message, $context);
    }

    $this->callDepth--;
  }

  /**
   * {@inheritdoc}
   */
  public function setLoggers(array $loggers) {
    $this->loggers = $loggers;
  }

  /**
   * {@inheritdoc}
   */
  public function addLogger(LoggerInterface $logger, $priority = 0) {
    // Store it so we can pass it to potential new logger instances.
    $this->loggers[$priority][] = $logger;
  }

  /**
   * Sorts loggers according to priority.
   *
   * @return array
   *   An array of sorted loggers by priority.
   */
  protected function sortLoggers() {
    $sorted = [];
    krsort($this->loggers);

    foreach ($this->loggers as $loggers) {
      $sorted = array_merge($sorted, $loggers);
    }
    return $sorted;
  }

}
