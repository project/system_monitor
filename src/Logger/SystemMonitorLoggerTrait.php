<?php

namespace Drupal\system_monitor\Logger;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\system_monitor\SystemMonitorLogLevel;

/**
 * Functions to translate between RFC and System Monitor logger status values.
 */
trait SystemMonitorLoggerTrait {

  /**
   * Returns an array of RFC to System Monitor numeric IDs.
   *
   * @return array
   *   The array of mapped RFC to System Monitor values.
   */
  public function getRfcToSystemMonitorMap() {
    $rfc_levels = RfcLogLevel::getLevels();
    $level_mapping = [];

    foreach ($rfc_levels as $level => $level_name) {
      if ($level === 5 || $level === 4) {
        $level_mapping[$level] = SystemMonitorLogLevel::STATUS_WARNING;
      }
      elseif ($level < 4) {
        $level_mapping[$level] = SystemMonitorLogLevel::STATUS_ERROR;
      }
      else {
        $level_mapping[$level] = SystemMonitorLogLevel::STATUS_OK;
      }
    }
    return $level_mapping;
  }

  /**
   * Returns an array of System Monitor to RFC numeric IDs.
   *
   * It is likely that there will be RFC values skipped during this process
   * since, by default, many RFC values can be mapped to a single System Monitor
   * status, but the "array_flip" method allows for the most conservative, yet
   * accurate status to be returned by System Monitor when no severity level
   * is passed through.
   *
   * @return array
   *   The array of mapped RFC to System Monitor values.
   */
  public function mapSystemMonitorToRfc() {
    $level_mapping = $this->getRfcToSystemMonitorMap();
    return array_flip($level_mapping);
  }

  /**
   * Maps an RFC severity level to Status Monitor value.
   *
   * @param int $level
   *   The RFC severity level to map.
   *
   * @return int
   *   The System Monitor value that goes with this RFC level.
   */
  public function getSystemMonitorStatus($level) {
    $mapped_statuses = $this->getRfcToSystemMonitorMap();
    if (isset($mapped_statuses[$level])) {
      return $mapped_statuses[$level];
    }
    return SystemMonitorLogLevel::STATUS_WARNING;
  }

  /**
   * Used to format log values and organize them by system monitor and task.
   *
   * @param array $logs
   *   The log data to organize.
   *
   * @return array
   *   The organized log data.
   */
  public function reorderLogData(array $logs) {
    $events = [];
    foreach ($logs as $log) {
      $task_key = $log['monitor_type'] . '|' . $log['monitor_task_name'];
      if (empty($events[$task_key])) {
        if ($log['status'] == 1) {
          continue;
        }
        $events[$task_key] = [
          'active_key' => 0,
          'events' => [
            [
              'title' => $log['short_description'],
              'reported_date' => $log['first_report'] ? $log['timestamp'] : NULL,
              'resolved_date' => NULL,
              'status' => $log['status'],
              'messages' => [
                $log['timestamp'] => [
                  'timestamp' => $log['timestamp'],
                  'title' => $log['short_description'],
                  'description' => $log['description'],
                  'status' => $log['status'],
                ],
              ],
            ],
          ],
        ];
      }
      else {
        $active_key = $events[$task_key]['active_key'];
        // If this is a new event, and it's not a standalone "healthy" event,
        // then set up the data array.
        if (empty($events[$task_key]['events'][$active_key])) {
          if ($log['status'] > 1) {
            $events[$task_key]['events'][$active_key] = [
              'title' => $log['short_description'],
              'reported_date' => $log['first_report'] ? $log['timestamp'] : NULL,
              'resolved_date' => NULL,
              'status' => $log['status'],
              'messages' => [],
            ];
          }
          else {
            continue;
          }
        }
        elseif ($events[$task_key]['events'][$active_key]['status'] < $log['status']) {
          $events[$task_key]['events'][$active_key]['status'] = $log['status'];
        }

        $previous_log = end($events[$task_key]['events'][$active_key]['messages']);

        $events[$task_key]['events'][$active_key]['messages'][$log['timestamp']] = [
          'timestamp' => $log['timestamp'],
          'title' => $log['short_description'],
          'description' => $log['description'],
          'status' => $log['status'],
        ];

        // If the error was resolved as of this log record, then move the
        // active_key to the next entry.
        if ($previous_log && $log['status'] != $previous_log['status'] && $log['status'] == 1) {
          $events[$task_key]['events'][$active_key]['resolved_date'] = $log['timestamp'];
          $events[$task_key]['active_key'] += 1;
        }
      }
    }
    foreach ($events as &$event) {
      unset($event['active_key']);
    }
    return $events;
  }

}
