<?php

namespace Drupal\system_monitor\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\system_monitor\SystemMonitorTaskManager;
use Drush\Commands\DrushCommands;

/**
 * System monitor drush commands.
 */
class SystemMonitorCommands extends DrushCommands {

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The system monitor task manager.
   *
   * @var \Drupal\system_monitor\SystemMonitorTaskManager
   */
  protected $systemMonitorManager;

  /**
   * The config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * SystemMonitorCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manger
   *   Entity Type manager.
   * @param \Drupal\system_monitor\SystemMonitorTaskManager $system_monitor_manager
   *   System monitor task manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manger, SystemMonitorTaskManager $system_monitor_manager, ConfigFactoryInterface $config_factory) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manger;
    $this->systemMonitorManager = $system_monitor_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Turn system monitor cron on.
   *
   * @command sm:cron-on
   * @aliases sm:cron,sm-cron-on
   */
  public function cronOn() {
    $this->setMonitorConfig('cron', 1);
  }

  /**
   * Turn system monitor cron off.
   *
   * @command sm:cron-off
   * @aliases sm:cron-off,sm-cron-off
   */
  public function cronOff() {
    $this->setMonitorConfig('cron', 0);
  }

  /**
   * Turn system monitor debug on.
   *
   * @command sm:debug-on
   * @aliases sm:debug,sm-debug-on
   */
  public function debugOn() {
    $this->setMonitorConfig('debug', 1);
  }

  /**
   * Turn system monitor debug off.
   *
   * @command sm:debug-off
   * @aliases sm:debug-off,sm-debug-off
   */
  public function debugOff() {
    $this->setMonitorConfig('debug', 0);
  }

  /**
   * Enable system monitor.
   *
   * @param string $system_monitor_id
   *   The system monitor id to update.
   *
   * @command sm:monitor-enable
   * @aliases sm:mon-en,sm-monitor-enable
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function monitorEnable($system_monitor_id) {
    $this->setMonitorStatus($system_monitor_id, TRUE);
  }

  /**
   * Disable system monitor.
   *
   * @param string $system_monitor_id
   *   The system monitor id to update.
   *
   * @command sm:monitor-disable
   * @aliases sm:mon-dis,sm-monitor-disable
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function monitorDisable($system_monitor_id) {
    $this->setMonitorStatus($system_monitor_id, FALSE);
  }

  /**
   * Run active monitor(s) or task(s).
   *
   * @param string $type
   *   The system monitor grouping to run.
   * @param string $list_to_run
   *   The comma separated list of monitor(s) or task(s) to run.
   * @param array $options
   *   Associative array of option values from cli, aliases, config, etc.
   *
   * @usage drush sm:run monitor
   *   Run all active monitors.
   * @usage drush sm:run monitor foo,bar
   *   Run active monitors named foo and bar.
   * @usage drush sm:run monitor --tags=foo-bar
   *   Run all active monitors tagged 'foo-bar'.
   * @usage drush sm:run task
   *   Run all active tasks.
   *
   * @command sm:run
   * @aliases sm:run,sm-run
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function run($type = '', $list_to_run = '', array $options = ['tags' => NULL]) {
    $this->output()->writeln(dt('Let\'s begin...'));
    if (empty($type)) {
      $this->output()->writeln(dt("Type value must be either 'monitor' or 'task'. Please resubmit with one of these values."));
      return;
    }

    $tags = $options['tags'];
    $tags = !empty($tags) ? explode(',', $tags) : [];
    $list = !empty($list_to_run) ? explode(',', $list_to_run) : [];

    $approved_type = in_array($type, ['monitor', 'task']);
    if (!$approved_type) {
      $this->output()->writeln(dt("Type value must be either 'monitor' or 'task'. Please resubmit with one of these values."));
      return;
    }

    switch ($type) {
      case 'monitor':
        $this->output()->writeln(dt('We be running monitor(s).'));
        $this->systemMonitorManager->runSystemMonitorTasks($list, $tags);
        break;

      case 'task':
        $this->output()->writeln(dt('We be running task(s).'));
        $this->systemMonitorManager->runSystemMonitorTasks($list, [], $tags);
        break;
    }

    $this->output()->writeln(dt('All task(s) have completed.'));
  }

  /**
   * Set the config for the field and value passed.
   *
   * @param string $config_name
   *   The name of the config to update.
   * @param bool $value
   *   The boolean value to set config in config.
   */
  public function setMonitorConfig($config_name, $value) {
    $system_monitor_config = $this->configFactory
      ->getEditable('system_monitor.config');

    $system_monitor_config->set($config_name, $value);
    $system_monitor_config->save();
    $cron_value = $system_monitor_config->get($config_name);

    $this->output()->writeln(dt("System monitor config property '@config' is set to '@value'.", ['@config' => $config_name, '@value' => $cron_value]));
  }

  /**
   * Set monitor status by monitor id.
   *
   * @param string $monitor_id
   *   System monitor to update.
   * @param bool $status_value
   *   Value to set field to.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function setMonitorStatus($monitor_id, $status_value) {

    $monitor = $this->entityTypeManager
      ->getStorage('system_monitor')
      ->load($monitor_id);

    if (!empty($monitor)) {
      if ($status_value) {
        $monitor->enable();
        $msg = 'enabled';
      }
      else {
        $monitor->disable();
        $msg = 'disabled';
      }
      // $monitor->save();
      $this->output()->writeln(dt("System monitor '@monitor_id' status set to '@msg'.", ['@monitor_id' => $monitor_id, '@msg' => $msg]));
      return;
    }

    $this->output()->writeln(dt("System monitor '@monitor_id' does not exist, please try again.", ['@monitor_id' => $monitor_id]));

  }

}
