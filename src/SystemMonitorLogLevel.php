<?php

namespace Drupal\system_monitor;

/**
 * System Monitor log levels values.
 */
class SystemMonitorLogLevel {

  /**
   * Status severity -- Default status when monitor has not yet been run.
   */
  const STATUS_PENDING = 0;

  /**
   * Status severity -- All checks completed successfully.
   */
  const STATUS_OK = 1;

  /**
   * Status severity -- Warning when soft status check fails to complete.
   */
  const STATUS_WARNING = 2;

  /**
   * Status severity -- Error when hard status check fails to complete.
   */
  const STATUS_ERROR = 3;

  /**
   * Status severity -- An error occurred but was resolved after the fact.
   *
   * This status only occurs on the status page or when manually set. The
   * system, by default, will never use this status.
   */
  const STATUS_RESOLVED = 4;

  /**
   * Returns array of status values suitable for use in a select list.
   *
   * @return array
   *   Returns an array of the statuses available in the system.
   */
  public static function getStatuses() {
    $statuses = [
      self::STATUS_PENDING => t('Pending'),
      self::STATUS_OK => t('OK'),
      self::STATUS_WARNING => t('Warning'),
      self::STATUS_ERROR => t('Error'),
      self::STATUS_RESOLVED => t('Resolved'),
    ];
    return $statuses;
  }

}
