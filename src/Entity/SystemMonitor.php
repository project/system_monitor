<?php

namespace Drupal\system_monitor\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\system_monitor\SystemMonitorInterface;

/**
 * Defines the System Monitor configuration entity.
 *
 * @ConfigEntityType(
 *   id = "system_monitor",
 *   label = @Translation("System Monitor"),
 *   handlers = {
 *     "list_builder" = "Drupal\system_monitor\SystemMonitorListBuilder",
 *     "form" = {
 *       "add" = "Drupal\system_monitor\Form\SystemMonitorForm",
 *       "edit" = "Drupal\system_monitor\Form\SystemMonitorForm",
 *       "delete" = "Drupal\system_monitor\Form\SystemMonitorDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\system_monitor\SystemMonitorHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer system monitor",
 *   config_prefix = "monitor",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/system-monitor/{system_monitor}",
 *     "add-form" = "/admin/config/system/system-monitor/add",
 *     "edit-form" = "/admin/config/system/system-monitor/{system_monitor}",
 *     "delete-form" = "/admin/config/system/system-monitor/{system_monitor}/delete",
 *     "collection" = "/admin/config/system/system-monitor"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "icon",
 *     "tags",
 *     "notify_statuses",
 *     "active"
 *   }
 * )
 */
class SystemMonitor extends ConfigEntityBase implements SystemMonitorInterface {

  /**
   * The System Monitor configuration ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The System Monitor configuration label.
   *
   * @var string
   */
  protected $label;

  /**
   * The brief description of the system monitor.
   *
   * @var string
   */
  protected $description;

  /**
   * The icon for the system monitor.
   *
   * @var string
   */
  protected $icon;

  /**
   * The tags used to group monitors when running them via drush commands.
   *
   * @var array
   */
  protected $tags = [];

  /**
   * The states of the monitor that trigger notifications.
   *
   * @var array
   */
  protected $notify_statuses = [];

  /**
   * Get the brief description of the system monitor.
   *
   * @return string
   *   The description of the System Monitor.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Get the icon for the system monitor.
   *
   * @return string
   *   The icon for the System Monitor.
   */
  public function getIcon() {
    return $this->icon;
  }

  /**
   * Get the tags to use when running the system monitor via drush commands.
   *
   * @param bool $return_string
   *   Whether or not to return an array or a string. Default to an array.
   *
   * @return array|string
   *   The tags for the System Monitor.
   */
  public function getMonitorTags($return_string = FALSE) {
    if ($return_string) {
      $tags = implode("\n", $this->tags);
      return $tags;
    }
    return $this->tags;
  }

  /**
   * Get the states of the monitor that trigger notifications.
   *
   * @return array
   *   The array of states that trigger notifications for this monitor.
   */
  public function getNotifyStatuses() {
    return $this->notify_statuses;
  }

  /**
   * The config name for this monitor's task config.
   *
   * @return string
   *   The ID for the config of this monitor's tasks.
   */
  public function getTaskConfigId() {
    return 'system_monitor.' . $this->id() . '.task_config';
  }

}
