<?php

/**
 * @file
 * Contains the code to generate the custom drush commands for system_monitor.
 */

/**
 * Implements hook_drush_command().
 */
function system_monitor_drush_command() {
  $items = [];
  $items['sm-cron-on'] = [
    'description' => 'Turn system monitor cron on.',
    'aliases' => ['sm:cron'],
  ];
  $items['sm-cron-off'] = [
    'description' => 'Turn system monitor cron off.',
    'aliases' => ['sm:cron-off'],
  ];
  $items['sm-debug-on'] = [
    'description' => 'Turn system monitor debug on.',
    'aliases' => ['sm:debug'],
  ];
  $items['sm-debug-off'] = [
    'description' => 'Turn system monitor debug off.',
    'aliases' => ['sm:debug-off'],
  ];
  $items['sm-monitor-enable'] = [
    'description' => 'Enable system monitor.',
    'aliases' => ['sm:mon-en'],
    'arguments' => [
      'system_monitor_id' => 'The system monitor id to update.',
    ],
  ];
  $items['sm-monitor-disable'] = [
    'description' => 'Disable system monitor.',
    'aliases' => ['sm:mon-dis'],
    'arguments' => [
      'system_monitor_id' => 'The system monitor id to update.',
    ],
  ];
  $items['sm-run'] = [
    'description' => 'Run active monitor(s) or task(s).',
    'aliases' => ['sm:run'],
    'arguments' => [
      'type' => 'The system monitor grouping to run.',
      'list_to_run' => 'The comma separated list of monitor(s) or task(s) to run.',
    ],
    'examples' => [
      'drush sm:run monitor' => 'Run all active monitors.',
      'drush sm:run monitor foo,bar' => 'Run active monitors named foo and bar.',
      'drush sm:run monitor --tags=foo-bar' => 'Run all active monitors tagged \'foo-bar\'.',
      'drush sm:run task' => 'Run all active tasks.',
    ],
    'options' => [
      'tags' => [
        'description' => 'Comma separated string of tag(s) used to group multiple monitors or tasks.',
        'example-value' => 'foo-bar',
      ],
    ],
  ];
  return $items;
}

/**
 * Run system monitor.
 *
 * @param string $type
 *   Type of system monitor.
 * @param string $list
 *   List of machine ids.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function drush_system_monitor_sm_run($type = '', $list = '') {
  $tags = drush_get_option('tags', '');
  $tags = !empty($tags) ? explode(',', $tags) : [];
  $list = !empty($list) ? explode(',', $list) : [];

  drush_print(dt('Let\'s begin...'));
  if (empty($type)) {
    drush_print(dt('Type value must be either \'monitor\' or \'task\'. Please resubmit with one of these values.'));
    return;
  }

  $approved_type = in_array($type, ['monitor', 'task']);
  if (!$approved_type) {
    drush_print(dt('Type value must be either \'monitor\' or \'task\'. Please resubmit with one of these values.'));
    return;
  }

  /** @var \Drupal\system_monitor\SystemMonitorTaskManager $system_monitor_plugin_manager */
  $system_monitor_plugin_manager = \Drupal::service('plugin.manager.system_monitor_task');

  switch ($type) {
    case 'monitor':
      drush_print(dt('We be running monitor(s).'));
      $system_monitor_plugin_manager->runSystemMonitorTasks($list, $tags);
      break;

    case 'task':
      drush_print(dt('We be running task(s).'));
      $system_monitor_plugin_manager->runSystemMonitorTasks($list, [], $tags);
      break;
  }

  drush_print(dt('All task(s) have completed.'));
}

/**
 * Enable system monitor cron.
 */
function drush_system_monitor_sm_cron_on() {
  system_monitor_set_config('cron', 1);
}

/**
 * Disable system monitor cron.
 */
function drush_system_monitor_sm_cron_off() {
  system_monitor_set_config('cron', 0);
}

/**
 * Enable system monitor debugging.
 */
function drush_system_monitor_sm_debug_on() {
  system_monitor_set_config('debug', 1);
}

/**
 * Disable system monitor debugging.
 */
function drush_system_monitor_sm_debug_off() {
  system_monitor_set_config('debug', 0);
}

/**
 * Enable system monitor.
 */
function drush_system_monitor_sm_monitor_enable($monitor_id = '') {
  system_monitor_set_monitor_status($monitor_id, TRUE);
}

/**
 * Disable system monitor.
 */
function drush_system_monitor_sm_monitor_disable($monitor_id = '') {
  system_monitor_set_monitor_status($monitor_id, FALSE);
}

/**
 * Set monitor status by monitor id.
 *
 * @param string $monitor_id
 *   System monitor to update.
 * @param string $status_value
 *   Value to set field to.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function system_monitor_set_monitor_status($monitor_id, $status_value) {

  /** @var \Drupal\system_monitor\SystemMonitorInterface $monitor */
  $monitor = \Drupal::service('entity_type.manager')
    ->getStorage('system_monitor')
    ->load($monitor_id);

  if (!empty($monitor)) {
    if ($status_value) {
      $monitor->enable();
      $msg = 'enabled';
    }
    else {
      $monitor->disable();
      $msg = 'disabled';
    }
    $monitor->save();
    drush_print(dt("System monitor '@monitor_id' status set to '@msg'.", ['@monitor_id' => $monitor_id, '@msg' => $msg]));
    return;
  }

  drush_print(dt("System monitor '@monitor_id' does not exist, please try again.", ['@monitor_id' => $monitor_id]));
}

/**
 * Set the config for the field and value passed.
 *
 * @param string $config_name
 *   The name of the config to update.
 * @param int $value
 *   A value of 0 or 1 to set config value.
 */
function system_monitor_set_config($config_name, $value) {
  /** @var $system_monitor_config \Drupal\Core\Config\Config */
  $system_monitor_config = \Drupal::service('config.factory')
    ->getEditable('system_monitor.config');

  $system_monitor_config->set($config_name, $value);
  $system_monitor_config->save();
  $cron_value = $system_monitor_config->get($config_name);

  drush_print(dt("System monitor config property '@config' is set to '@value'.", ['@config' => $config_name, '@value' => $cron_value]));
}
