<?php

namespace Drupal\Tests\system_monitor\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides the base class for web tests for Search API.
 */
abstract class SystemMonitorBrowserTestBase extends BrowserTestBase {

  /**
   * Modules to enable for this test.
   *
   * @var string[]
   */
  public static $modules = [
    'system_monitor',
  ];

  /**
   * An admin user used for this test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * The number of meta refresh redirects to follow, or NULL if unlimited.
   *
   * @var int|null
   */
  protected $maximumMetaRefreshCount = NULL;

  /**
   * The number of meta refresh redirects followed during ::drupalGet().
   *
   * @var int
   */
  protected $metaRefreshCount = 0;

  /**
   * The permissions of the admin user.
   *
   * @var string[]
   */
  protected $adminUserPermissions = [
    'view the administration theme',
    'access administration pages',
    'administer system monitor',
  ];

  /**
   * A user without Status Page admin permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $unauthorizedUser;

  /**
   * The anonymous user used for this test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $anonymousUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Get the entity type manager class.
    $this->entityTypeManager = $this->container->get('entity_type.manager');
  }

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_shipment_type',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Creates or loads a system monitor.
   *
   * @return \Drupal\system_monitor\SystemMonitorInterface
   *   A system monitor.
   */
  public function getTestMonitor() {
    $storage = $this->entityTypeManager->getStorage('system_monitor');
    $system_monitor = $storage->load('webtest_system_monitor');
    if (!$system_monitor) {
      $system_monitor = $storage->create([
        'id' => 'webtest_system_monitor',
        'label' => 'WebTest system monitor',
        'description' => 'WebTest system monitor description',
        'monitor_group' => 'default',
        'notify_states' => [
          'error',
        ],
      ]);
      $system_monitor->save();
    }

    return $system_monitor;
  }

}
