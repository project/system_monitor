<?php

namespace Drupal\Tests\system_monitor\Functional;

use Drupal\Core\Url;

/**
 * Tests the System Monitor overview page.
 *
 * @group system_monitor
 */
class OverviewPageTest extends SystemMonitorBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
  ];

  /**
   * The path of the overview page.
   *
   * @var string
   */
  protected $overviewPageUrl = '';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Create the users used for the tests.
    $this->adminUser = $this->drupalCreateUser($this->adminUserPermissions);
    $this->unauthorizedUser = $this->drupalCreateUser(['access administration pages']);
    $this->anonymousUser = $this->drupalCreateUser();

    $this->drupalLogin($this->adminUser);
    $this->overviewPageUrl = 'admin/config/system/system-monitor';
  }

  /**
   * Tests system monitor operations in the overview page.
   */
  public function testOperations() {
    $system_monitor = $this->getTestMonitor();

    $this->drupalGet($this->overviewPageUrl);
    $this->assertResponse(200);
    $url_params = [
      'system_monitor' => $system_monitor->id(),
    ];
    $url_options = [
      'query' => [
        'destination' => Url::fromRoute('entity.system_monitor.collection')->toString(),
      ],
    ];
    $edit_url = Url::fromRoute('entity.system_monitor.edit_form', $url_params, $url_options)->toString();
    $delete_url = Url::fromRoute('entity.system_monitor.delete_form', $url_params, $url_options)->toString();

    // Check for an "add" link on the collection page.
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalGet($this->overviewPageUrl);
    $this->assertSession()->responseContains('/admin/config/system/system-monitor/add');
    $this->assertSession()->pageTextContains('Add System Monitor');

    // Check that admins can access the edit/delete buttons.
    $this->assertSession()->responseContains("<a href=\"$edit_url\">Edit</a>");
    $this->assertSession()->responseContains("<a href=\"$delete_url\">Delete</a>");

  }

  /**
   * Tests that the overview has the correct permissions set.
   */
  public function testOverviewPermissions() {
    $this->drupalGet('admin/config');
    $this->assertSession()->pageTextContains('System Monitor');

    $this->drupalGet($this->overviewPageUrl);
    $this->assertResponse(200);

    $this->drupalLogin($this->unauthorizedUser);
    $this->drupalGet($this->overviewPageUrl);
    $this->assertResponse(403);
  }

}
