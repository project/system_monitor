<?php

namespace Drupal\Tests\system_monitor\Unit\Logger;

use Drupal\Tests\UnitTestCase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Driver\mysql\Insert;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\system_monitor\Logger\SystemMonitorDbLogger;

/**
 * @coversDefaultClass \Drupal\system_monitor\Logger\SystemMonitorDbLogger
 * @group system_monitor
 */
class SystemMonitorDbLoggerTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $container = new ContainerBuilder();
    \Drupal::setContainer($container);

  }

  /**
   * Setup the System Monitor DB log to allow varying the query result.
   */
  public function setupMockSystemMonitorDbLogger($query_return_type) {
    $query = $this->getMockBuilder(Insert::class)
      ->setMethods([
        'fields',
        'execute',
      ])
      ->disableOriginalConstructor()
      ->getMock();
    $query->expects($this->any())
      ->method('fields')
      ->willReturnSelf();
    $query->expects($this->any())
      ->method('execute')
      ->will($query_return_type);

    $connection = $this->getMockBuilder(Connection::class)
      ->setMethods(['insert'])
      ->disableOriginalConstructor()
      ->getMockForAbstractClass();
    $connection->expects($this->any())
      ->method('insert')
      ->willReturn($query);

    return $this->getMockBuilder(SystemMonitorDbLogger::class)
      ->setMethods()
      ->setConstructorArgs([
        $connection,
      ])
      ->getMock();
  }

  /**
   * @covers ::log
   */
  public function testLogWithoutMonitorTaskName() {
    $test_level = 1;
    $test_message = 'Test';
    $context = [
      'monitor_type' => 'test_type',
    ];
    $system_monitor_log = $this->setupMockSystemMonitorDbLogger($this->returnValue([]));
    $this->assertNull($system_monitor_log->log($test_level, $test_message, $context));
  }

  /**
   * @covers ::log
   */
  public function testLogWithoutMonitorType() {
    $test_level = 1;
    $test_message = 'Test';
    $context = [
      'monitor_task_name' => 'test_task_name',
    ];
    $system_monitor_log = $this->setupMockSystemMonitorDbLogger($this->returnValue([]));
    $this->assertNull($system_monitor_log->log($test_level, $test_message, $context));
  }

  /**
   * @covers ::log
   */
  public function testLogWithMinimumRequiredValues() {
    $test_level = 1;
    $test_message = 'Test';
    $context = [
      'monitor_task_name' => 'test_task_name',
      'monitor_type' => 'test_type',
      'timestamp' => 1,
    ];
    $system_monitor_log = $this->setupMockSystemMonitorDbLogger($this->returnValue([]));
    $this->assertNull($system_monitor_log->log($test_level, $test_message, $context));
  }

  /**
   * Test when a non-database related error occurs during the query process.
   *
   * @covers ::log
   */
  public function testLogWithGeneralException() {
    $test_level = 1;
    $test_message = 'Test';
    $context = [
      'monitor_task_name' => 'test_task_name',
      'monitor_type' => 'test_type',
      'timestamp' => 1,
    ];
    $system_monitor_log = $this->setupMockSystemMonitorDbLogger($this->throwException(new \Exception()));

    $e = NULL;
    try {
      $system_monitor_log->log($test_level, $test_message, $context);
    }
    catch (\Exception $e) {
    }
    $this->assertInstanceOf(\Exception::class, $e);
  }

}
