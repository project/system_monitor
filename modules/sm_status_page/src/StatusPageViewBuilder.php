<?php

namespace Drupal\sm_status_page;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Drupal\system_monitor\SystemMonitorLogLevel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * View builder handler for status pages.
 */
class StatusPageViewBuilder extends EntityViewBuilder {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The status page utility service.
   *
   * @var \Drupal\sm_status_page\StatusPageUtility
   */
  protected $statusPageUtility;

  /**
   * Constructs a new FeedViewBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\sm_status_page\StatusPageUtility $status_page_utility
   *   The status page utility.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   */
  public function __construct(EntityTypeInterface $entity_type, DateFormatter $date_formatter, EntityRepositoryInterface $entity_repository, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, RequestStack $request_stack, StatusPageUtility $status_page_utility, Registry $theme_registry) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->statusPageUtility = $status_page_utility;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('date.formatter'),
      $container->get('entity.repository'),
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('request_stack'),
      $container->get('sm_status_page.utility'),
      $container->get('theme.registry')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $status_page, $view_mode = 'full', $langcode = NULL) {
    /* @var $entity \Drupal\sm_status_page\StatusPageInterface */
    $this->statusPageUtility->setStatusPage($status_page);
    $cards = $this->statusPageUtility->getMonitorData();

    // Update cards to group tasks by status so tasks in error states are shown
    // at the top of the list.
    $status_code = SystemMonitorLogLevel::STATUS_OK;
    foreach ($cards as &$card) {
      if ($status_code < $card['status']) {
        $status_code = $card['status'];
      }
    }
    $status_value = 'is Healthy';
    if ($status_code === SystemMonitorLogLevel::STATUS_WARNING || $status_code === SystemMonitorLogLevel::STATUS_ERROR) {
      $status_value = 'is Experiencing an Issue';
    }
    $system_status = $this->statusPageUtility->convertStatusForPage($status_code);

    $last_checked_date = $this->dateFormatter->format($this->statusPageUtility->getUpdatedDate('last_updated'), 'sm_status_page_date');

    $content = [];
    if ($view_mode == 'issues') {
      $content = $this->buildIssuePageContent($status_page);
    }
    else {
      $content = $this->buildMainPageContent($status_page);
    }

    $build = [
      'view_mode' => $view_mode,
      'header' => [
        'title' => $this->t('@label', [
          '@label' => $status_page->label(),
        ]),
        'title_status' => $this->t('@label @value', [
          '@label' => $status_page->label(),
          '@value' => $status_value,
        ]),
        'title_image' => [
          '#theme' => 'image',
          '#uri' => $this->statusPageUtility->getStatusPageIcon(),
        ],
        'menu' => [
          [
            'text' => $this->t('Dashboard'),
            'url' => $status_page->getPath(),
            'class' => $view_mode == 'full' ? ' class="active"' : '',
          ],
          [
            'text' => $this->t('Issues'),
            'url' => $status_page->getPath() . '/issues',
            'class' => $view_mode == 'issues' ? ' class="active"' : '',
          ],
        ],
        'status' => $system_status,
        'last_checked' => $this->t('Since @date', ['@date' => $last_checked_date]),
      ],
      'content' => $content,
      'footer' => [
        'logo' => $status_page->getLogoUrl(),
        'menu' => $status_page->getFooterLinks(TRUE),
        'copyright' => $this->t('Copyright &copy; @year @name', [
          '@year' => date('Y', time()),
          '@name' => $status_page->getCopyright(),
        ]),
      ],
    ];
    $build = $this->addRenderArrayElements($build);
    return $build;
  }

  /**
   * Build the summary content for the main page.
   *
   * @param \Drupal\Core\Entity\EntityInterface $status_page
   *   The status page interface.
   *
   * @return array
   *   The array of values for the content section.
   */
  public function buildMainPageContent(EntityInterface $status_page) {
    /* @var $entity \Drupal\sm_status_page\StatusPageInterface */
    $this->statusPageUtility->setStatusPage($status_page);
    $cards = $this->statusPageUtility->getMonitorData();

    // Update cards to group tasks by status so tasks in error states are shown
    // at the top of the list.
    $status_code = SystemMonitorLogLevel::STATUS_OK;
    foreach ($cards as &$card) {
      $grouped_tasks = [];
      foreach ($card['detail']['tasks'] as $task) {
        if ($task['status'] > $status_code) {
          if (!isset($grouped_tasks['error'])) {
            $grouped_tasks['error']['title'] = $this->t('Tasks with errors');
          }
          $grouped_tasks['error']['tasks'][] = $task;
        }
        else {
          if (!isset($grouped_tasks['ok'])) {
            $grouped_tasks['ok']['title'] = $this->t('Healthy tasks');
          }
          $grouped_tasks['ok']['tasks'][] = $task;
        }
      }
      ksort($grouped_tasks);
      $card['grouped_tasks'] = $grouped_tasks;
    }

    // Only load the latest 5 issues for the main page.
    $issue_data = $this->statusPageUtility->getIssueListData(5);

    // Get the number of days of historical data to display as set by the user
    // on the admin configuration page.
    $time_limit = $status_page->getIncidentTimeLimit();
    $timeline_data = $this->getFormattedTimelineData($time_limit);

    return [
      'timeline' => [
        'title' => $this->t('History'),
        'timespan' => $this->t('Past @interval', [
          '@interval' => $time_limit,
        ]),
        'statuses' => [
          'outage' => $this->t('Downtime'),
          'issue' => $this->t('Issue'),
          'healthy' => $this->t('Healthy'),
        ],
        'events' => $timeline_data,
      ],
      'cards' => [
        'title' => $this->t('Monitors'),
        'description_label' => $this->t('Description'),
        'items' => $cards,
      ],
      'history' => [
        'title' => $this->t('Issue History'),
        'issues' => $issue_data['results'],
        'no_issues' => $this->t('No reported issues in the past @interval.', ['@interval' => $time_limit]),
        'link' => [
          'text' => $this->t('View all issue history'),
          'url' => $status_page->getPath() . '/issues',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildIssuePageContent(EntityInterface $status_page) {

    $request = $this->requestStack->getCurrentRequest();
    $query_string = $request->query;
    $page = !empty($query_string->get('page')) ? $query_string->get('page') : 1;
    $size = !empty($query_string->get('size')) ? $query_string->get('size') : 5;

    $issue_data = $this->statusPageUtility->getIssueListData($size, $page - 1);
    $page_data = $issue_data;
    unset($page_data['results']);

    $total_pages = ceil($issue_data['total'] / $size);
    $query_values = [
      'page' => &$page,
      'size' => &$size,
    ];
    $this->validateIssueQueryString($query_values, $total_pages);

    // Build the pager values for the page.
    $possible_pages = range(1, $total_pages);
    if ($total_pages < 7) {
      $pages = $possible_pages;
    }
    elseif (($total_pages - $page) < 3) {
      $pages = array_slice($possible_pages, -5);
    }
    elseif (($page - 3) < 0) {
      $pages = array_slice($possible_pages, 0, 5);
    }
    else {
      $pages = array_slice($possible_pages, $page - 3, 5);
    }

    $pager = [
      'back' => $page - 1 >= 1 ? $page - 1 : NULL,
      'pages' => $pages,
      'query' => [
        'page' => $page,
        'size' => $size,
      ],
      'next' => $page + 1 <= $total_pages ? $page + 1 : NULL,
      'total_pages' => $total_pages,
      'url' => $status_page->getPath() . '/issues',
    ];

    return [
      'history' => [
        'title' => $this->t('Issue History'),
        'issues' => $issue_data['results'],
        'no_issues' => $this->t('No results returned.'),
      ],
      'pager' => $pager,
    ];
  }

  /**
   * Validate the query strings passed through the browser.
   *
   * @param array $query_string
   *   The query strings (page and size) from the browser.
   * @param int $total_pages
   *   The total number of possible pages for the issues listing.
   *
   * @return array
   *   The array of validated query string parameters.
   */
  private function validateIssueQueryString(array $query_string = [], $total_pages) {
    $defaults = [
      'page' => 1,
      'size' => 5,
    ];
    $default_sizes = [5, 15, 25, 50];
    foreach ($query_string as $key => $value) {
      if (!is_numeric($value)) {
        $query_string[$key] = $defaults[$key];
      }
      else {
        $query_string[$key] = (int) $value;
      }
    }
    if ($query_string['page'] > $total_pages) {
      $query_string['page'] = $total_pages;
    }
    elseif ($query_string['page'] < 0) {
      $query_string['page'] = 0;
    }

    if (!in_array($query_string['size'], $default_sizes)) {
      $query_string['size'] = 5;
    }
    return $query_string;
  }

  /**
   * Format all timeline data into sections that can be rendered.
   *
   * @param int $time_limit
   *   The number of days of events to include for the timeline.
   *
   * @return array
   *   The array of values to pass through to the front end.
   */
  private function getFormattedTimelineData($time_limit) {
    $total_days = (int) substr($time_limit, strpos($time_limit, '_'));
    $timeline_data = $this->statusPageUtility->getTimelineData($total_days);
    $percent_unit = 100 / $total_days;
    $events = [];
    $zero_incident_streak = 0;
    $incident_streak = 0;
    $incident_data = [];
    foreach ($timeline_data as $timeline_day) {
      // If the current day doesn't have any records, then there were no
      // recorded incidents that day.
      if (empty($timeline_day)) {
        $zero_incident_streak += 1;
        // If the incident streak is greater than zero, then this is a return to
        // stable status after an incident. Close the incident streak and add
        // the appropriate markup.
        if ($incident_streak > 0) {
          $width = $percent_unit * $incident_streak;
          $incident_width = 100 / $incident_streak;
          foreach ($incident_data as $key => $data_values) {
            $incident_data[$key]['#attributes']['style'] = "width:{$incident_width}%";
          }
          $events[] = [
            '#type' => 'container',
            '#attributes' => [
              'data-incident-streak' => $incident_streak,
              'class' => [
                'timeline__incident-status',
              ],
              'style' => "width:{$width}%",
            ],
            'incidents' => $incident_data,
          ];
          $incident_streak = 0;
          $incident_data = [];
        }
      }
      else {
        $incident_streak += 1;
        // This is the first instance of an incident after an "ok" status.
        if ($zero_incident_streak > 0) {
          $width = $percent_unit * $zero_incident_streak;
          $events[] = [
            '#type' => 'container',
            '#attributes' => [
              'data-incident-streak' => $zero_incident_streak,
              'class' => [
                'timeline__stable-status',
              ],
              'style' => "width:{$width}%",
            ],
          ];
          $zero_incident_streak = 0;
        }
        // There is an incident log for this day, but it was okay during the
        // previous day. Open a new incident data record.
        $error_status = 2;
        foreach ($timeline_day as $event) {
          if ($event['status'] > $error_status) {
            $error_status = $event['status'];
          }
        }
        $status_value = $this->statusPageUtility->convertStatusForPage($error_status);
        $incident_width = 100 / count($timeline_day);
        $incident_data[$incident_streak] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'timeline__incident--wrapper',
              'timeline__incident-status--' . strtolower($status_value),
            ],
          ],
          'highlight' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'timeline__highlight',
              ],
            ],
          ],
        ];
        $incident_render_array = [];
        foreach ($timeline_day as $incident) {
          $value_array = [
            'title' => [
              '#type' => 'container',
              '#attributes' => [
                'class' => [
                  'timeline__incident-title',
                ],
              ],
              'value' => $incident['title'],
            ],
          ];
          $reported_label = ($incident['has_reported_date']) ? 'Reported' : 'Logged before';
          $value_array['reported_date'] = [
            '#type' => 'container',
            'value' => $this->t('<strong>@label:</strong> @date', [
              '@label' => $reported_label,
              '@date' => $incident['reported_date_formatted'],
            ]),
          ];
          if (!empty($incident['resolved_date_formatted'])) {
            $value_array['resolved_date'] = [
              '#type' => 'container',
              'value' => $this->t('<strong>Resolved:</strong> @date', ['@date' => $incident['resolved_date_formatted']]),
            ];
          }
          $status = $this->statusPageUtility->convertStatusForPage($incident['status']);
          $incident_render_array[] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'timeline__message',
                'timeline__message--' . strtolower($status),
              ],
            ],
            'messages' => $value_array,
          ];
        }
        $incident_data[$incident_streak][] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'timeline__tooltip',
            ],
          ],
          'data' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'tooltiptext',
              ],
            ],
            'data' => $incident_render_array,
          ],
        ];
      }
    }
    // The last streak was error statuses, add them to the timeline.
    if (!empty($incident_streak)) {
      $width = $percent_unit * $incident_streak;
      $incident_width = 100 / $incident_streak;
      foreach ($incident_data as $key => $data_values) {
        $incident_data[$key]['#attributes']['style'] = "width:{$incident_width}%";
      }
      $events[] = [
        '#type' => 'container',
        '#attributes' => [
          'data-incident-streak' => $incident_streak,
          'class' => [
            'timeline__incident-status',
          ],
          'style' => "width:{$width}%",
        ],
        'incidents' => $incident_data,
      ];
    }
    // The last streak was stable statuses, add them to the timeline build.
    if (!empty($zero_incident_streak)) {
      $width = $percent_unit * $zero_incident_streak;
      $events[] = [
        '#type' => 'container',
        '#attributes' => [
          'data-incident-streak' => $zero_incident_streak,
          'class' => [
            'timeline__stable-status',
          ],
          'style' => "width:{$width}%",
        ],
      ];
    }
    $timeline_end = $this->statusPageUtility->getUpdatedDate('timeline');
    $date_labels = $this->buildDayLabels($time_limit, $timeline_end);
    $day_labels = [];
    foreach ($date_labels as $date_label) {
      $width = $date_label['days'] / $total_days * 100;
      $class = ($date_label['days'] > 3) ? 'border' : 'no-border';
      $day_labels[] = [
        '#type' => 'container',
        '#attributes' => [
          'data-day-streak' => $date_label['days'],
          'class' => [
            'timeline__month-label',
            'timeline__month-label--' . $class,
          ],
          'style' => "width:{$width}%;",
        ],
        'day_label' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => ($date_label['days'] > 3) ? $date_label['month'] : '',
        ],
      ];
    }
    return [
      'items' => $events,
      'months' => $day_labels,
    ];
  }

  /**
   * Builds the array of day labels for the timeline.
   */
  private function buildDayLabels($time_limit, $timeline_end) {
    $months = [];
    $date = new DrupalDateTime("-{$time_limit}");
    $date->setTime(0, 0);
    $months[] = [
      'month' => $date->format('M'),
      'start' => $date->getTimestamp(),
      'date' => $date,
    ];
    $next_month = clone $date;
    $next_month->modify("first day of next month");
    while ($next_month->getTimestamp() < $timeline_end) {
      $months[] = [
        'month' => $next_month->format('M'),
        'start' => $next_month->getTimestamp(),
        'date' => clone $next_month,
      ];
      $next_month->modify("first day of next month");
    }
    $current_date = new DrupalDateTime();
    $current_date->setTime(0, 0);
    foreach ($months as $index => &$month) {
      if (isset($months[$index + 1])) {
        $diff = $months[$index + 1]['date']->diff($month['date']);
      }
      else {
        $diff = $current_date->diff($month['date']);
      }
      $month['days'] = $diff->days;
    }
    return $months;
  }

  /**
   * Adds markup to an array to ensure that it is ready for rendering.
   *
   * @param array $variables
   *   The variables array to use when adding in markup information.
   * @param string $section
   *   The optional section of the status page that is being built.
   */
  private function addRenderArrayElements(array $variables, $section = '') {
    $content_section = '';
    foreach ($variables as $key => &$var) {
      if (is_array($var)) {
        // Don't process any attributes variables.
        if ($key === '#attributes' || $key === 'pager') {
          continue;
        }
        $content_section = $section . '|' . $key;
        $var = $this->addRenderArrayElements($var, $content_section);
      }
      else {
        if (strpos($key, '#') !== 0) {
          switch ($key) {
            case 'status':
              $status = $this->statusPageUtility->convertStatusForPage($var);
              if (strpos($section, 'content|history|issues')) {
                $resolution = 'Resolved';
                $active_status = 'Closed';
                if ($status != 'Resolved') {
                  $resolution = 'Unresolved';
                  $active_status = 'Open Issue';
                }
                $var = [
                  'resolution' => $resolution,
                  'active_status' => $this->t('@active_status', ['@active_status' => $active_status]),
                ];
              }
              else {
                $value = strtolower($status);
                $var = [
                  '#type' => 'html_tag',
                  '#tag' => 'div',
                  '#value' => $status,
                  '#attributes' => [
                    'class' => [
                      'status',
                      "status--$value",
                    ],
                  ],
                ];
              }
              break;

            case 'image':
              $var = [
                '#type' => 'markup',
                '#markup' => $var,
              ];
              break;

            case 'description':
              $var = [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#value' => nl2br($var),
                '#attributes' => [
                  'class' => [
                    'description',
                  ],
                ],
              ];
              break;

            case 'timestamp':
              $var = [
                '#type' => 'markup',
                '#markup' => $this->dateFormatter->format($var, 'sm_status_page_date'),
              ];
              break;

            default:
              $var = [
                '#type' => 'markup',
                '#markup' => $var,
              ];
          }
        }
      }
    }
    return $variables;
  }

}
