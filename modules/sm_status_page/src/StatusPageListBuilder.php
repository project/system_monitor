<?php

namespace Drupal\sm_status_page;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of Status page entities.
 */
class StatusPageListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Machine name');
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');
    $header['path'] = $this->t('Path');
    $header['roles'] = $this->t('Allowed Roles');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $status_page) {
    $link = [
      '#type' => 'link',
      '#url' => Url::fromUserInput($status_page->getPath()),
      '#title' => $status_page->label(),
    ];
    $row['id'] = $status_page->id();
    $row['label']['data'] = $link;
    $row['description'] = $status_page->getDescription();

    // Also apply a link to the path value.
    $link['#title'] = $status_page->getPath();
    $row['path']['data'] = $link;

    $row['roles']['data'] = [
      '#theme' => 'item_list',
      '#items' => $status_page->getRoles(),
    ];
    $row['status'] = empty($status_page->status()) ? $this->t('Disabled') : $this->t('Enabled');
    return $row + parent::buildRow($status_page);
  }

}
