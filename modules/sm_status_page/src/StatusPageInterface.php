<?php

namespace Drupal\sm_status_page;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Status page entities.
 */
interface StatusPageInterface extends ConfigEntityInterface {

  /**
   * Get the brief description of the system monitor.
   *
   * @return string
   *   The description of the System Monitor.
   */
  public function getDescription();

  /**
   * Get the monitors for the status page.
   *
   * @return array
   *   The monitor ids of the monitors tracked on this status page.
   */
  public function getMonitors();

  /**
   * Get the path for the status page.
   *
   * @return string
   *   The URL for the status page.
   */
  public function getPath();

  /**
   * Get the roles for the status page.
   *
   * @return array
   *   The ids of the roles allowed to access the status page.
   */
  public function getRoles();

}
