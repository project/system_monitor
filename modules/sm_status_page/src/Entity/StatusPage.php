<?php

namespace Drupal\sm_status_page\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\sm_status_page\StatusPageInterface;

/**
 * Defines the Status page entity.
 *
 * @ConfigEntityType(
 *   id = "sm_status_page",
 *   label = @Translation("Status Page"),
 *   label_collection = @Translation("Status Pages"),
 *   label_singular = @Translation("Status Page"),
 *   label_plural = @Translation("Status Pages"),
 *   label_count = @PluralTranslation(
 *     singular = "@count status page",
 *     plural = "@count status pages",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\sm_status_page\StatusPageListBuilder",
 *     "view_builder" = "Drupal\sm_status_page\StatusPageViewBuilder",
 *     "form" = {
 *       "add" = "Drupal\sm_status_page\Form\StatusPageForm",
 *       "edit" = "Drupal\sm_status_page\Form\StatusPageForm",
 *       "delete" = "Drupal\sm_status_page\Form\StatusPageDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\sm_status_page\StatusPageHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "status_page",
 *   admin_permission = "administer status pages",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/system-monitor/status-page/{sm_status_page}",
 *     "add-form" = "/admin/config/system/system-monitor/status-page/add",
 *     "edit-form" = "/admin/config/system/system-monitor/status-page/{sm_status_page}/edit",
 *     "delete-form" = "/admin/config/system/system-monitor/status-page/{sm_status_page}/delete",
 *     "collection" = "/admin/config/system/system-monitor/status-page"
 *   },
 *   render_cache = TRUE,
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "path",
 *     "logo_url",
 *     "footer_links",
 *     "copyright",
 *     "monitors",
 *     "incident_time_limit",
 *     "roles",
 *     "active"
 *   }
 * )
 */
class StatusPage extends ConfigEntityBase implements StatusPageInterface {

  /**
   * The Status page ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Status page label.
   *
   * @var string
   */
  protected $label;

  /**
   * The footer menu links for the Status Page.
   *
   * @var string
   */
  protected $footer_links;

  /**
   * The url to the Status Page url.
   *
   * @var string
   */
  protected $logo_url;

  /**
   * The name to show in the footer for the copyright message.
   *
   * @var string
   */
  protected $copyright;

  /**
   * The brief description of the status page.
   *
   * @var string
   */
  protected $description;

  /**
   * The Status page path.
   *
   * @var string
   */
  protected $path;

  /**
   * The monitors to track on the page.
   *
   * @var array
   */
  protected $monitors = [];

  /**
   * The historical time limit value for the status page logic.
   *
   * @var int
   */
  protected $incident_time_limit;

  /**
   * The roles allowed to access this status page.
   *
   * @var array
   */
  protected $roles = [];

  /**
   * Get the copyright name of the system monitor.
   *
   * @return string
   *   The copyright name of the System Monitor.
   */
  public function getCopyright() {
    return $this->copyright;
  }

  /**
   * Get the brief description of the system monitor.
   *
   * @return string
   *   The description of the System Monitor.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Get the footer links for the Status Page.
   *
   * @return string
   *   The footer links for the Status Page.
   */
  public function getFooterLinks($parsed = FALSE) {
    if (!$parsed) {
      return $this->footer_links;
    }
    $links = explode("\n", $this->footer_links);
    $map = ['text', 'url'];
    foreach ($links as &$link) {
      $link_array = explode('||', $link);
      // If the link doesn't have both a URL and text, then empty it and move
      // on to the next entry.
      if (count($link_array) < 2) {
        $link = [];
        continue;
      }
      $link_array = array_filter($link_array, 'trim');
      $link = array_combine($map, $link_array);
    }
    $links = array_filter($links);
    return $links;
  }

  /**
   * Get the url to the logo for the status page.
   *
   * @return string
   *   The url to the logo of the status page.
   */
  public function getLogoUrl() {
    return $this->logo_url;
  }

  /**
   * Get the path for the status page.
   *
   * @return string
   *   The URL for the status page.
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Get the monitors for the status page.
   *
   * @return array
   *   The monitor ids of the monitors tracked on this status page.
   */
  public function getMonitors() {
    return $this->monitors;
  }

  /**
   * Get the limit on historical data to show for the status page.
   *
   * @return string
   *   The limit on historical data to show for the status page.
   */
  public function getIncidentTimeLimit() {
    return $this->incident_time_limit;
  }

  /**
   * Get the roles for the status page.
   *
   * @return array
   *   The ids of the roles allowed to access the status page.
   */
  public function getRoles() {
    return $this->roles;
  }

}
