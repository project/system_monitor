<?php

namespace Drupal\sm_status_page;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the sm_status_page entity type.
 *
 * @see \Drupal\sm_status_page\Entity\StatusPage
 */
class StatusPageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $status_page, $operation, AccountInterface $account) {
    $access = AccessResult::forbidden();
    // In case a status page is active, only allow access if a user has
    // administrative permission or the roles that were configured in the
    // status page's settings.
    if ($operation === 'view' && $status_page->status()) {
      $admin_access = AccessResult::allowedIfHasPermissions($account, ['view any status page', $this->entityType->getAdminPermission()], 'OR');
      $roles = array_filter($status_page->getRoles());
      if (!empty($roles)) {
        $user_roles = $account->getRoles();
        $user_has_roles = !empty(array_intersect($roles, $user_roles));
        $access = AccessResult::allowedIf($user_has_roles);
        $access->orIf($admin_access);
        $is_auth = $account->isAuthenticated();
        if ($is_auth) {
          $access->orIf(AccessResult::allowedIf(in_array('authenticated', $roles)));
        }
        else {
          $access->orIf(AccessResult::allowedIf(in_array('anonymous', $roles)));
        }
      }
      else {
        $access = $admin_access;
      }
      return $access->addCacheableDependency($status_page);
    }

    // Allow update access with a specific or admin permission.
    if ($operation === 'update') {
      $access = AccessResult::allowed();
      $access->andIf(AccessResult::allowedIfHasPermissions($account, ['edit any status page', $this->entityType->getAdminPermission()], 'OR'));
    }
    // Only users with admin permission can delete status pages.
    elseif ($operation === 'delete') {
      $access = AccessResult::allowed();
      $access->andIf(AccessResult::allowedIfHasPermission($account, ['delete any status page', $this->entityType->getAdminPermission()], 'OR'));
    }

    return $access->addCacheableDependency($status_page);
  }

  /**
   * {@inheritdoc}
   */
  public function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'create status page')->orIf(parent::checkCreateAccess($account, $context, $entity_bundle));
  }

}
