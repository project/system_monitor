<?php

namespace Drupal\sm_status_page;

use DateInterval;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\system_monitor\Logger\SystemMonitorLogger;
use Drupal\system_monitor\SystemMonitorLogLevel;
use Drupal\system_monitor\SystemMonitorUtility;

/**
 * Provides utility functions to gather data for Status Pages.
 */
class StatusPageUtility {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The menu link overrides config object.
   *
   * @var \Drupal\sm_status_page\StatusPageInterface
   */
  protected $statusPage;

  /**
   * Cache service object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The system monitor logger.
   *
   * @var \Drupal\system_monitor\Logger\SystemMonitorLogger
   */
  protected $systemMonitorLogger;

  /**
   * The system monitor utility service.
   *
   * @var \Drupal\system_monitor\SystemMonitorUtility
   */
  protected $systemMonitorUtility;

  /**
   * Constructs a SystemMonitorUtility object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory object.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger interface.
   * @param \Drupal\system_monitor\Logger\SystemMonitorLogger $system_monitor_logger
   *   The system monitor logger service.
   * @param \Drupal\system_monitor\SystemMonitorUtility $system_monitor_utility
   *   The utility service for system monitor.
   */
  public function __construct(CacheBackendInterface $cache, Connection $connection, ConfigFactoryInterface $config_factory, DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger, SystemMonitorLogger $system_monitor_logger, SystemMonitorUtility $system_monitor_utility) {
    $this->cache = $cache;
    $this->configFactory = $config_factory;
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger->get('sm_status_page');
    $this->systemMonitorLogger = $system_monitor_logger;
    $this->systemMonitorUtility = $system_monitor_utility;
  }

  /**
   * Builds the cache key for the given value.
   *
   * @param string $value
   *   The cache key string value for the status page.
   *
   * @return string
   *   The full cache key string value.
   */
  private function getCacheKey($value) {
    return 'sm_status_page.' . $this->statusPage->id() . '.' . $value;
  }

  /**
   * Update the last checked date for the given key value.
   *
   * @param string $partial_cache_key
   *   The  value to use when building the cache key.
   * @param int $timestamp
   *   The integer timestamp to set.
   * @param int $expire
   *   The expiration timespan (in seconds) for the cache entry.
   */
  public function setCacheValue($partial_cache_key, $timestamp, $expire) {
    $cache_key = $this->getCacheKey($partial_cache_key);
    $this->cache->set($cache_key, $timestamp, $expire, [
      'sm_status_page',
      'sm_status_page.' . $this->statusPage->id(),
      $cache_key,
    ]);
  }

  /**
   * Gets the last time the System Monitor data was updated.
   *
   * @return timestamp
   *   The timestamp of the last time the data was checked.
   */
  public function getUpdatedDate($value) {
    $cached_date_value = $this->cache->get('system_monitor.last_updated');
    if (empty($cached_date_value->data)) {
      $date = time();
      $this->cache->set('system_monitor.last_updated', $date);
    }
    else {
      $date = $cached_date_value->data;
    }
    return (int) $date;
  }

  /**
   * Sets the status page to use for the utility.
   *
   * @param \Drupal\sm_status_page\Entity\StatusPage|string $status_page
   *   The ID or the actual Status Page object to use with the utility.
   */
  public function setStatusPage($status_page) {
    if (!is_object($status_page)) {
      $this->statusPage = $this->entityTypeManager->getStorage('sm_status_page')->load($status_page);
    }
    else {
      $this->statusPage = $status_page;
    }
  }

  /**
   * Convert the status in the logs to statuses for the status page.
   *
   * @param int $status_code
   *   The integer code for the status.
   * @param bool $is_rfc
   *   Whether or not the incoming status is an RFC level.
   *
   * @return string
   *   The convereted status value.
   */
  public function convertStatusForPage($status_code, $is_rfc = FALSE) {
    if ($is_rfc) {
      $statuses = $this->systemMonitorLogger->mapRfcToSystemMonitor();
      $status_code = $statuses[$status_code];
    }
    switch ($status_code) {
      case SystemMonitorLogLevel::STATUS_WARNING:
        $status = 'Issue';
        break;

      case SystemMonitorLogLevel::STATUS_ERROR;
        $status = 'Downtime';
        break;

      case SystemMonitorLogLevel::STATUS_RESOLVED;
        $status = 'Resolved';
        break;

      default:
        $status = 'Healthy';
    }
    return $status;
  }

  /**
   * Get all active monitors configured on a status page.
   *
   * @param bool $load_objects
   *   Whether or not the monitors should be loaded before being returned.
   *
   * @return \Drupal\system_monitor\SystemMonitorInterface[]
   *   An array of the active configured system monitors for the status page.
   */
  protected function getConfiguredMonitors($load_objects = TRUE) {
    $system_monitor_storage = $this->entityTypeManager->getStorage('system_monitor');
    $query = $system_monitor_storage->getQuery()
      ->condition('status', 1);
    $monitors = $this->statusPage->getMonitors();
    if (!empty($monitors)) {
      $query->condition('id', $monitors, 'IN');
    }
    $configured_monitors = $query->execute();
    return $load_objects ? $system_monitor_storage->loadMultiple($configured_monitors) : $configured_monitors;
  }

  /**
   * Get the data for the timeline on the status page.
   *
   * @param int $total_days
   *   The total number of day sto show in the historical timeline.
   *
   * @return array
   *   The render array for the status page timeline.
   */
  public function getTimelineData($total_days = 7) {
    $cache_key = $this->getCacheKey('timeline');
    $cached_timeline = $this->cache->get($cache_key);

    // Check if this detail has already been built in cache. If so, then return
    // the cached value. If not, then continue building out the timeline.
    if (!empty($cached_timeline)) {
      return $cached_timeline->data;
    }

    $timeline_start_date = new DrupalDateTime("-{$total_days} days");
    $timeline_start_date->setTime(0, 0);
    $timeline_start_timestamp = $timeline_start_date->getTimestamp();
    $data = $this->systemMonitorLogger->buildHistoricalEventsByMonitor($this->getConfiguredMonitors(FALSE), $timeline_start_timestamp);
    $timeline_data = [];

    // Use DrupalDateTime to prebuild an array of daily timestamps that begin
    // at midnight and take into account daylight savings time.
    for ($countdown = $total_days; $countdown > 0; $countdown--) {
      $index = new DrupalDateTime("-{$countdown} days");
      $index->setTime(0, 0);
      $timeline_data[$index->getTimestamp()] = [];
    }
    $index->add(new DateInterval('P1D'));
    $index->setTime(0, 0);

    foreach ($data as $task => $events) {
      if (!empty($events['events'])) {
        foreach ($events['events'] as $event) {
          $message_timestamps = array_keys($event['messages']);
          $messages = array_values($event['messages']);
          $reported_date = !empty($event['reported_date']) ? $event['reported_date'] : $timeline_start_timestamp;
          $resolved_date = !empty($event['resolved_date']) ? $event['resolved_date'] : NULL;
          $event_data = [
            'title' => $event['title'],
            'reported_date' => $reported_date,
            'reported_date_formatted' => $this->dateFormatter->format((int) $reported_date, 'sm_status_page_date'),
            'has_reported_date' => !empty($event['reported_date']),
            'resolved_date' => $resolved_date,
            'resolved_date_formatted' => !empty($resolved_date) ? $this->dateFormatter->format((int) $resolved_date, 'sm_status_page_date') : NULL,
            'has_resolved_date' => !empty($event['resolved_date']),
            'first_message_date' => !empty($event['messages'][0]) ? $event['messages'][0]['timestamp'] : $timeline_start_timestamp,
          ];
          $date = new DrupalDateTime();
          $last_key = count($messages) - 1;
          foreach ($messages as $message_key => $message) {
            $timestamp = $message_timestamps[$message_key];
            $date->setTimestamp($timestamp);
            $date->setTime(0, 0);
            $current_timestamp = $date->getTimestamp();
            $next_message_timestamp = ($message_key < $last_key) ? $message_timestamps[$message_key + 1] : $resolved_date;
            while ($current_timestamp < $next_message_timestamp) {
              if (!isset($timeline_data[$current_timestamp][$task]) || $message['status'] >= $timeline_data[$current_timestamp][$task]['status']) {
                $timeline_data[$current_timestamp][$task] = array_merge($event_data, ['status' => $message['status']]);
              }
              $date->add(new DateInterval('P1D'));
              $date->setTime(0, 0);
              $current_timestamp = $date->getTimestamp();
            }
          }
        }
      }
    }

    // Set the timeline data to update at midnight (or as close to it) so the
    // timeline contains all issue data from previous days.
    $midnight = new DrupalDateTime('midnight');
    if ($midnight->getTimestamp() < time()) {
      $midnight->add(new DateInterval('P1D'));
      $midnight->setTime(0, 0);
    }
    $diff = $midnight->getTimestamp() - time();
    $this->setCacheValue('timeline', $timeline_data, $diff);
    $this->setCacheValue('dates.timeline', time(), $diff);
    return $timeline_data;
  }

  /**
   * Get the data for the monitor cards on the status page.
   *
   * @return array
   *   The array of monitor card data.
   */
  public function getMonitorData() {
    $monitors = $this->getConfiguredMonitors();
    if (empty($monitors)) {
      return [];
    }
    return $this->systemMonitorUtility->getIncidentData($monitors);
  }

  /**
   * Get the data for the monitor cards on the status page.
   *
   * @return array
   *   The array of monitor card data.
   */
  public function getStatusPageIcon() {
    if ($this->systemMonitorUtility->hasActiveIncident()) {
      $icon = 'web-and-computer/bug.svg';
    }
    else {
      $icon = 'business-and-marketing/thumbs-up.svg';
    }
    return $this->systemMonitorUtility->getFullIconPath($icon);
  }

  /**
   * Get the data for the issue list on the status page.
   *
   * @param int|bool $total_issues
   *   The total number of issues to return in the request.
   *
   * @return array
   *   The list of issue data.
   */
  public function getIssueListData($total_issues = 25, $page_number = 0) {
    $cache_key = $this->getCacheKey('issue_list');
    $cached_issue_list = $this->cache->get($cache_key);
    $results = [];

    // Check if this detail has already been built in cache. If so, then return
    // the value from cache. If not, then continue building out the timeline.
    if (FALSE) {
    // if (!empty($cached_issue_list)) {
      $results = $cached_issue_list->data;
    }
    else {
      $monitors = $this->getConfiguredMonitors(FALSE);
      $data = $this->systemMonitorLogger->getMonitorLogs($monitors);
      $ordered_events = [];
      foreach ($data as &$task_data) {
        foreach ($task_data['events'] as &$event) {
          $event = end($event['messages']);
          if ($event['status'] == 1) {
            $event['status'] = 4;
          }
          $ordered_events[$event['timestamp']][] = $event;
        }
      }
      krsort($ordered_events);

      // Cache for 15 minutes.
      $cache_time = 60 * 15;
      $this->setCacheValue('issue_list', $ordered_events, $cache_time);
      $this->setCacheValue('dates.issue_list', time(), $cache_time);
      $results = $ordered_events;
    }

    $grand_total = count($results);

    if ($total_issues) {
      $start = $page_number * $total_issues;
      $results = array_slice($results, $start, $total_issues);
    }

    return [
      'results' => $results,
      'total' => $grand_total,
      'page' => $page_number,
      'size' => $total_issues,
    ];
  }

}
