<?php

namespace Drupal\sm_status_page\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\system_monitor\SystemMonitorUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class StatusPageForm.
 */
class StatusPageForm extends EntityForm {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The router builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * The system monitor utility service.
   *
   * @var \Drupal\system_monitor\SystemMonitorUtility
   */
  protected $systemMonitorUtility;

  /**
   * Constructs a new StatusPageForm.
   *
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The site path.
   * @param Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The router builder service.
   * @param \Drupal\system_monitor\SystemMonitorUtility $system_monitor_utility
   *   The system monitor utility.
   */
  public function __construct(RequestStack $request_stack, RouteBuilderInterface $router_builder, SystemMonitorUtility $system_monitor_utility) {
    $this->requestStack = $request_stack;
    $this->routerBuilder = $router_builder;
    $this->systemMonitorUtility = $system_monitor_utility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('router.builder'),
      $container->get('system_monitor.utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $status_page = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $status_page->label(),
      '#description' => $this->t("Label for the status page."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $status_page->id(),
      '#machine_name' => [
        'exists' => '\Drupal\sm_status_page\Entity\StatusPage::load',
      ],
      '#disabled' => !$status_page->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#rows' => 2,
      '#default_value' => $status_page->getDescription(),
      '#description' => $this->t('Description of the status page configuration.'),
      '#required' => TRUE,
    ];

    $base_url = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $form['footer'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Footer settings'),
      'copyright' => [
        '#type' => 'textfield',
        '#title' => $this->t('Copyright'),
        '#default_value' => $status_page->getCopyright(),
        '#description' => $this->t('The name to show at the end of the copyright message.'),
        '#required' => TRUE,
      ],
      'logo_url' => [
        '#type' => 'textfield',
        '#title' => $this->t('Footer Logo URL'),
        '#description' => $this->t('Enter the path to the logo to display in the footer. This shoudld be a local path on the server.'),
        '#default_value' => $status_page->getLogoUrl(),
        '#size' => 45,
        '#field_prefix' => $base_url,
      ],
      'footer_links' => [
        '#type' => 'textarea',
        '#title' => $this->t('Footer Links'),
        '#description' => $this->t('Enter links that should be rendered in the footer in the format:<br/><code>Link text||https://www.google.com<br/>Internal link text||/relative-link</code>'),
        '#default_value' => $status_page->getFooterLinks(),
        '#size' => 45,
        '#required' => TRUE,
      ],
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Status Page Path'),
      '#description' => $this->t('Enter the URL path where the status page should be accessible.'),
      '#field_prefix' => $base_url,
      '#default_value' => $status_page->getPath(),
      '#size' => 45,
      '#required' => TRUE,
    ];

    $form['incident_time_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Historical Data Time Limit'),
      '#description' => $this->t('Select how far back the system should show when displaying historical incident data.'),
      '#default_value' => $status_page->getIncidentTimeLimit(),
      '#options' => [
        '7 days' => $this->t('7 days'),
        '15 days' => $this->t('15 days'),
        '30 days' => $this->t('30 days'),
        '60 days' => $this->t('60 days'),
        '90 days' => $this->t('90 days'),
        '120 days' => $this->t('120 days'),
      ],
      '#required' => TRUE,
    ];

    $form['monitors_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tracked Monitors'),
      '#description' => $this->t('Select the system monitors to include on this Status Page. Selecting nothing will include all active system monitors.'),
      '#prefix' => '<div id="monitors-wrapper>',
      '#suffix' => '</div>',
    ];

    // Get any monitors that have already been set.
    $monitors = $this->systemMonitorUtility->getActiveMonitorOptions();
    $form['monitors_wrapper']['monitors'] = [
      '#type' => 'checkboxes',
      '#options' => $monitors,
      '#default_value' => $status_page->getMonitors(),
    ];

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed Roles'),
      '#description' => $this->t('Select the roles that should be allowed to access this status page. Selecting nothing will make it inaccessible to anyone except users with "administer status page" or "view all status pages" permissions.'),
      '#default_value' => $status_page->getRoles(),
      '#options' => user_role_names(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $status_page->status(),
      '#description' => $this->t('Uncheck this to disable this status page.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $paths = [
      'path' => 'Status Page Path',
      'logo_url' => 'Footer Logo URL',
    ];
    foreach ($paths as $field => $label) {
      $path = $form_state->getValue($field);
      if (!empty($path) && strpos($path, '/') !== 0) {
        $form_state->setErrorByName($field, "The {$label} must being with a forward slash.");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status_page = $this->entity;
    $status_page->set('roles', array_filter($status_page->getRoles()));
    $status_page->set('monitors', array_filter($status_page->getMonitors()));
    $status = $status_page->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Status page.', [
          '%label' => $status_page->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Status page.', [
          '%label' => $status_page->label(),
        ]));
    }
    $this->routerBuilder->setRebuildNeeded();
    $form_state->setRedirectUrl($status_page->toUrl('collection'));
  }

}
