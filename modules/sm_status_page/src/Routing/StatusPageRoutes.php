<?php

namespace Drupal\sm_status_page\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines a route subscriber to register a url for serving status pages@.
 */
class StatusPageRoutes implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new StatusPageRoutes object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $routes = new RouteCollection();

    $page_storage = $this->entityTypeManager->getStorage('sm_status_page');
    $active_page_ids = $page_storage->getQuery()
      ->condition('status', 1)
      ->execute();

    $active_pages = $page_storage->loadMultiple($active_page_ids);

    foreach ($active_pages as $page_id => $status_page) {
      $routes->add("sm_status_page.$page_id", new Route(
        $status_page->getPath(),
        [
          '_entity_view' => 'sm_status_page.full',
          '_title' => $status_page->label(),
          'sm_status_page' => $status_page,
        ],
        [
          '_status_page_access_check' => 'TRUE',
        ]
      ));
      $routes->add("sm_status_page.$page_id.issues", new Route(
        $status_page->getPath() . '/issues',
        [
          '_entity_view' => 'sm_status_page.issues',
          '_title' => $status_page->label() . ': Issues',
          'sm_status_page' => $status_page,
        ],
        [
          '_status_page_access_check' => 'TRUE',
        ]
      ));
    }

    return $routes;

  }

}
