<?php

namespace Drupal\sm_status_page\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Default State Page Controller.
 */
class StatusPageAccessCheck implements AccessInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a SystemManager object.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(AccountInterface $current_user, RequestStack $request_stack) {
    $this->currentUser = $current_user;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('request_stack')
    );
  }

  /**
   * Access check to verify that the user has a company referenced.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Whether or not the user is allowed to access the route.
   */
  public function access() {
    $access = AccessResult::forbidden();

    $request = $this->requestStack->getCurrentRequest();
    $status_page = $request->attributes->get('sm_status_page');

    // In case a status page is active, only allow access if a user has
    // administrative permission or the roles that were configured in the
    // status page's settings.
    if (!empty($status_page) && $status_page->status()) {
      $admin_access = AccessResult::allowedIfHasPermissions($this->currentUser, ['view any status page', 'administer status pages'], 'OR');
      $roles = array_filter($status_page->getRoles());
      if (!empty($roles)) {
        $user_roles = $this->currentUser->getRoles();
        $user_has_roles = !empty(array_intersect($roles, $user_roles));
        $access = AccessResult::allowedIf($user_has_roles);
        $access->orIf($admin_access);
      }
      else {
        $access = $admin_access;
      }
    }

    return $access->addCacheableDependency($status_page);
  }

}
