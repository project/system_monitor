The Icons included with the Status Page module are pulled from the Mediacurrent
Icon Library. This library is open source and free for anyone to use.

https://github.com/mediacurrent/icon-library

To update the library, delete the `icons` directory and run:
`npm run install-icons`

The icons will be included in the official releases of System Monitor, but will
but updated periodically as needed.
